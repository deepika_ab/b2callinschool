<?php
require_once('../../config.php');
require_once($CFG->dirroot.'/local/businesssignup/form/index_form.php');
$context = context_system::instance();
global $CFG, $DB;
//require_login();
$PAGE->set_context($context);
$PAGE->set_url('/local/businesssignup/index.php');
//$PAGE->set_heading('Contact Us');
$PAGE->set_pagelayout('base');
$PAGE->set_title('Business Signup');
$PAGE->requires->jquery();
$PAGE->requires->css(new moodle_url('/local/businesssignup/css/style.css'));

echo $OUTPUT->header();

$businesssignupobj = new businesssignup_form();

if ($formdata = $businesssignupobj->get_data()) { 
        //send mail to admin/admins
        //$supportuser = core_user::get_support_user();
        //$supportuser->maildisplay = true;
        $to = 'info@allinschool.com';
        //$to = 'nihar.cbsh@gmail.com';
        //$from = "From: ".$formdata->name."<".$formdata->email.">";

        $body = '';
        $body .= 'Dear Admin<br/><br/>';
        $body .= 'Please find the below enquiry details<br/><br/>';
        $body .= '<body>
        <table style="font-family:Calibri; font-size:16px;" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
                <th colspan="2" align="center" bgcolor="#a21d22" style="padding:10px; color:#ffffff;" nowrap>Enquiry</th>
            </tr>
            <tr> 
              <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black; padding:10px; color:#583333;" nowrap>Name</td>
              <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->firstname.' '.$formdata->lastname . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Email</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->email . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Mobile No.</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->mobno . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Company</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->company . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Jobtitle</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->jobtitle . '</td>
            </tr>
            <tr>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;" nowrap>Message</td>
                <td align="center" bgcolor="#f9f9f9" style="border: 1px solid black;padding:10px; color:#583333;">' . $formdata->message . '</td>
            </tr>';
        $body .= '</tbody>
        </table></body>';
        $body .= '<p style="font-family: Calibri;"><br/>Regards,<br/>All IN School Team</p>';
        //mail($to,$subject,$body,$headers);
        //email_to_user($userobj, $supportuser, $subject, '', $body);
        block_outlook_calendar($formdata, $body);
        redirect(new moodle_url('/course/index.php'),'Thank you for contacting us. We will get back to you.', 10);
    //}
    
}

$businesssignupobj->display();

echo $OUTPUT->footer();

//Block outlook calendar
function block_outlook_calendar($formdata, $body){
    
    global $CFG, $DB;
    require_once($CFG->dirroot.'/lib/phpmailer/moodle_phpmailer.php');
    //$usertomailid = $DB->get_record('user', array('idnumber' => $usertoempid));
    //$progname = $DB->get_record('batchcode_support',array('id' => $trcalendarobj->progid))->name;
    //$bodycontent = get_calendar_publish_nomination_mail_body($trcalendarobj->id);
    $mail = new moodle_phpmailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'email-smtp.ap-southeast-1.amazonaws.com';                       // Specify main and backup server
    $mail->SetFrom('noreply@allinschool.com', 'Allinschool');
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'AKIA24FTY2RX5IAPOC26';                   // SMTP username
    $mail->Password = 'BDiBMIATjGR3PGP4Q0K18jl+JSd2cKVefXM7fmoa/Iv+';               // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted
    $mail->Port = 465;                                    //Set the SMTP port number - 587 for authenticated TLS
    $mail->addAddress($formdata->email);  // Add a recipient
    $mail->WordWrap = 500;                                 // Set word wrap to 50 characters
    $mail->isHTML(true);                                  // Set email format to HTML

    $subject = 'Business Signup';
    $mail->Subject = $subject;
    $mail->Body = $body; 
    $mail->IsHTML(true);  
    
    try {
        $mail->send();
    } catch (Exception $ex) {
        throw new moodle_exception('Error sending email');
    }
}
