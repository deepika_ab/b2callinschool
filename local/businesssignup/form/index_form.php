<?php

require_once("$CFG->libdir/formslib.php");
 
class businesssignup_form extends moodleform {
 
    function definition() {
        global $CFG;
        $mform = $this->_form; // Don't forget the underscore! 
        //$mform->addElement('header', 'contactus', get_string('contactus', 'local_contactus'));                
        $mform->addElement('html','<div class="row col-md-12">');
        $mform->addElement('html','<div class="col-md-6">');
        $mform->addElement('html','</div>');
        $mform->addElement('html','<div class="col-md-6">');
        $mform->addElement('text', 'firstname', '', array('placeholder' => 'Enter First Name'));
        $mform->setType('firstname', PARAM_TEXT);
        $mform->addRule('firstname', null, 'required');
        
        $mform->addElement('text', 'lastname', '', array('placeholder' => 'Enter Last Name'));
        $mform->setType('lastname', PARAM_TEXT);
        $mform->addRule('lastname', null, 'required');
        
        $mform->addElement('text', 'email', '', array('placeholder' => 'Enter Your Email'));
        $mform->setType('email', PARAM_EMAIL);
        $mform->addRule('email', null, 'required');
        
        $mform->addElement('text', 'mobno', '', array('placeholder' => 'Enter Your Phone Number'));
        $mform->setType('mobno', PARAM_ALPHANUM);
        $mform->addRule('mobno', null, 'required');
        
        $mform->addElement('text', 'company', '', array('placeholder' => 'Enter Company Name'));
        $mform->setType('company', PARAM_TEXT);
        $mform->addRule('company', null, 'required');
        
        $mform->addElement('text', 'jobtitle', '', array('placeholder' => 'Enter Job Title'));
        $mform->setType('jobtitle', PARAM_TEXT);
        $mform->addRule('jobtitle', null, 'required');
        
//        $mform->addElement('text', 'subject', '', array('placeholder' => ''));
//        $mform->setType('subject', PARAM_TEXT);
//        $mform->addRule('subject', null, 'required');
        
        $mform->addElement('textarea', 'message', '','wrap="virtual" rows="5" cols="50" placeholder="What are your training needs?"');
        $mform->setType('message', PARAM_RAW);
        $mform->addRule('message', null, 'required');
        $this->add_action_buttons($cancel = true, $submitlabel='Submit'); 
        
        $mform->addElement('html','</div>');
        $mform->addElement('html','</div>');

        
        
            
    }
    
    function validation($data, $files) {
        global $DB;
        
        $errors = array();
        
        if (empty(trim($data['firstname']))){
            $errors['firstname'] ="Please enter first name";
        }
        
        if (empty(trim($data['lastname']))){
            $errors['lastname'] ="Please enter last name";
        }
        
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
            $errors['email'] ="Please enter a valid email";
        }
        
        if (strlen($data['mobno']) > 10){
            $errors['mobno'] = "Please enter 10 digit mobile number";
        } else if (!filter_var($data['mobno'], FILTER_SANITIZE_NUMBER_INT)){
            $errors['mobno'] = "Please enter integer mobile number";
        }
        
        if (empty(trim($data['company']))){
            $errors['company'] ="Please enter company name";
        }
        
        if (empty(trim($data['jobtitle']))){
            $errors['jobtitle'] ="Please enter job title";
        }
        
        if (empty(trim($data['message']))){
            $errors['message'] ="Please enter message";
        }
        return $errors;
    }
} 

