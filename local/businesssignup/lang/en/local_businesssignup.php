<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file for local_mass_enroll, EN
 *
 * Language File         
 * Encoding     UTF-8
 *
 * @package     
 *
 * @copyright   
 * @author      
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later 
 */

$string['pluginname'] = 'Business Signup';
$string['name'] = 'Name';
$string['email'] = 'Email';
$string['mobno'] = 'Mobile Number';
$string['subject'] = 'Subject';
$string['message'] = 'Message';
$string['contactus'] = 'Contact Us';
