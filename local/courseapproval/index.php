<?php

require_once('../../config.php');
$context = context_system::instance();
global $CFG, $DB;
require_login();
$PAGE->set_context($context);
$PAGE->set_url('/local/courseapproval/index.php');
$PAGE->set_heading('Course Approval');
$PAGE->set_pagelayout('base');
$PAGE->set_title('Course Approval');
$PAGE->requires->jquery();
$PAGE->requires->js('/local/courseapproval/js/custom.js', true);
//$PAGE->requires->css(new moodle_url('/local/courseapproval/css/style.css'));


echo $OUTPUT->header();

$coursenotconfirmed = $DB->get_records_sql("SELECT c.id, c.fullname, c.timecreated,ca.confirmed, ca.createdby FROM {course} c LEFT JOIN {b2c_course_approval} ca ON ca.courseid = c.id");
$table = new html_table();
$table->head = array('Coursename', 'Createdby', 'Createdon', 'Status', 'Action');
$table->id = 'coursetobeconfirmed';
foreach($coursenotconfirmed as $coursedata){
    if($coursedata->confirmed == 0 OR !isset($coursedata->confirmed)){
        $userdata = $DB->get_record('user', array('id' => $coursedata->createdby));
        $table->data[] = array(
            html_writer::link($CFG->wwwroot.'/course/view.php?id='.$coursedata->id, $coursedata->fullname),
            fullname($userdata),
            userdate($coursedata->timecreated),
            ($coursedata->confirmed == 0) ? 'Not Confirmed' : 'Confirmed',
            '<button class="btn btn-primary courseconfirm" value="'.$coursedata->id.'">Confirm</button>'
        );
    }
}
echo html_writer::tag('div','',array('id' => 'status'));
echo html_writer::table($table);

echo $OUTPUT->footer();
