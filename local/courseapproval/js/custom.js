$(document).ready(function () {
//    // datatable load
//    var table1 = $('#coursetobeconfirmed').DataTable({
//        dom: 'Bfrtip',
//        buttons: [
//            'csv', 'excel', 'pdf'
//        ]
//    });
     $(".courseconfirm").click(function(e){
        $("#status").html('<div class="text-center"><img src=' + M.cfg.wwwroot + '/local/courseapproval/img/loader.gif'+' alt="loader"/><div>Please Wait</div></div>');
        var courseid = $(this).val();

        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "ajaxhandler.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send('sesskey=' + M.cfg.sesskey + '&mode=' + 'approvecourse'  + '&courseid=' + courseid);
        
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var response = JSON.parse(this.responseText);
                $("#status").empty();
                document.getElementById('status').innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert">' + response.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                window.location.replace(M.cfg.wwwroot+'/local/courseapproval/index.php');
            }
        };
        return false;
    });
});