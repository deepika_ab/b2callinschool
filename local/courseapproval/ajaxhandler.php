<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_courseapproval
 * @copyright  Nihar Das <nihar.cbsh@gmail.com>
 * @copyright  
 * @license    
 */


require('../../config.php');

require_login();
$mode = required_param('mode', PARAM_TEXT);
$context = context_system::instance();
// Start setting up the page
$params = array();
$PAGE->set_context($context);

switch ($mode) {
    case 'approvecourse':
        global $DB;
        $courseid = required_param('courseid', PARAM_INT);
        $coursedata = $DB->get_record('b2c_course_approval', array('courseid' => $courseid));
        $dataobj = new stdClass();
        
        try{
            if($coursedata){
                $dataobj->id = $coursedata->id;
                $dataobj->confirmed = 1;
                $DB->update_record('b2c_course_approval', $dataobj);
            } else {
                $dataobj->courseid = $courseid;
                $dataobj->confirmed = 1;
                $dataobj->createdby = $USER->id;
                $dataobj->timecreated = time();
                $dataobj->timemodified = null;
                $lastinsertid = $DB->insert_record('b2c_course_approval', $dataobj);
            }
            echo json_encode(['status' => true, 'message' => 'Course approved successfully']);
        } catch (Exception $ex) {
            echo json_encode(['status' => false, 'message' => 'Error occured']);
        }
        break;
        
    default:
        break;
}



