<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edwiser RemUI
 * @package   theme_remui
 * @copyright (c) 2020 WisdmLabs (https://wisdmlabs.com/) <support@wisdmlabs.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG, $DB, $PAGE, $USER, $SITE, $COURSE;


if (stripos($PAGE->url->get_path(), '/course/index.php') !== false) {
    
    require_once('common.php');
    //nihar
    $catid = optional_param('categoryid', null, PARAM_INT);
    //$context = context_coursecat::instance($catid);
    $categorydetail = $DB->get_record('course_categories', array('id'=>$catid));
    //$content = file_rewrite_pluginfile_urls($categorydetail->description, 'pluginfile.php', $context->id, 'coursecat', 'description', null);
    $coursecat = core_course_category::get($categorydetail->id);
    $chelper = new coursecat_helper();
    $content = html_writer::div($chelper->get_category_formatted_description($coursecat));

    // Generate page url.
    $pageurl = new moodle_url('/course/index.php');
    $mycourses  = optional_param('mycourses', 0, PARAM_INT);

    // Get the filters first.
    $filterdata = \theme_remui_coursehandler::get_course_filters_data();
    $templatecontext['hasregionmainsettingsmenu'] = !$OUTPUT->region_main_settings_menu();

    $templatecontext['categories'] = $filterdata['catdata'];
    $templatecontext['searchhtml'] = $filterdata['searchhtml'];

    $templatecontext['tabcontent'] = array();

    if (isloggedin()) {
        // Tab creation Content.
        $mycoursesobj = new stdClass();
        $mycoursesobj->name = 'mycourses';
        $mycoursesobj->text = get_string('mycourses', 'theme_remui');
        if ($mycourses) {
            $mycoursesobj->isActive = true;
        }
        $templatecontext['tabcontent'][] = $mycoursesobj;
    }

    $coursesobj = new stdClass();
    $coursesobj->name = 'courses';
    $coursesobj->text = get_string('courses', 'theme_remui');
    if (!$mycourses) {
        $coursesobj->isActive = true;
    }
    $templatecontext['tabcontent'][] = $coursesobj;

    $templatecontext['mycourses'] = $mycourses;
    $templatecontext['catimage'] = $content;
    if (\theme_remui\toolbox::get_setting('enablenewcoursecards')) {
        $templatecontext['latest_card'] = true;
    }

    echo $OUTPUT->render_from_template('theme_remui/coursearchive', $templatecontext);
} else {
    require_once('columns2.php');
}
