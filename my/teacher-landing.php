<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- Teachers landing page
 *
 * 
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');

redirect_if_major_upgrade_required();

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/teacher-landing.php', $params);
$PAGE->set_pagelayout('standard');
$PAGE->set_pagetype('About Us');
$PAGE->blocks->add_region('content');
$PAGE->set_title($pagetitle);
$PAGE->set_heading($header);
$PAGE->requires->css(new moodle_url('/my/css/style.css'));
$PAGE->requires->js(new moodle_url('js/jquery.min.js'));
$PAGE->requires->js(new moodle_url('js/custom.js'));

echo $OUTPUT->header();

$html = '<div class="main">
            <div class="banner new_banner">
                <div class="overlay"></div>
                <div class="right_wrap">
                    <h1>Make a Global Impact</h1>
                    <p>Learn anything, anywhere the easiest way.</p>
                    <div class="btn_wrap">
  
<a class="btn login red font-size-20" href="'.$CFG->wwwroot.'/login/signup.php?mode=teacher'.'">Become an Instructor</a>
                      
                    </div>
                </div>
            </div>

        </div>
        <div class="solu_wrap">
            <div class="head discover">
                <h1>Discover your potential</h1>
                <hr>
                       </div>
            <div class="solu_item_wrap dis_item">
                <div class="item_30">
                    <img src="images/icon.svg"/>
                    <h5>Follow a student</h5>
                    <p>Students can choose other classmates to follow. A feed of activities would appear based on
                        activities performed. E.g. Discussions, notes, assignment
                        completion.</p>
                </div>
                <div class="item_30">
                    <img src="images/icon.svg"/>

                    <h5>Plagiarism</h5>
                    <p>Calculation of the percentage of plagiarism of the assignments submitted by the students. Rules
                        can be set to automatically reject assignment suspected of
                        plagiarism.</p>
                </div>
                <div class="item_30">
                    <img src="images/icon.svg"/>

                    <h5>Automation</h5>
                    <p>Allows the automation of reports and certain recurring tasks. E. g. Automatically enroll students
                        who meet certain criteria in certain classes, based on rules.</p>
                </div>
            </div>
            
        </div>';
        echo format_text($html, FORMAT_HTML, array('trusted' => true, 'noclean' => true, 'filter' => true));
        
        echo $OUTPUT->footer();
        
