<?php
$coursedata = $DB->get_record('course', array('id' => $id));
$url = $CFG->wwwroot.'/course/view.php?id='.$id.'&redirectfrom=coursedetails';

?>
<div class="container-fluid">
    <div class="col-md-12 row pad0A">
        <div class="col-md-6 pad0A">
            <h1 class="cours_hd coursedetailsh1">
                <?php echo $coursedata->fullname ?>
            </h1>
            <h3 class="subhead"><?php echo $coursedata->description ?></h3>           
          
        </div>
        <div class="col-md-6 pad0A">
            <div class="videoUrl">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/ezbJwaLmOeM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="col-md-12 pad0A hrborderline row"></div>
    
    <div class="col-md-12 pad0A row">
        <div class="col-md-6 rightborderline">
            <div class="aboutus pad2T">
                Allinschool offers a structured one year certificate course 
                for professional where you can learn at your own pace. 
                This is a unique blended learning (E-learning and face-to-face contact sessions) for working professionals. 
            </div>
            <h3 class="objective commonheader">
                Objective                               
            </h3>
            <span>
                <p>
                    <?php echo $coursedata->summary ?>
                </p>
            </span>
<!--             <h3 class="youlearn commonheader">
                You will learn                              
            </h3>
            <span>
                In this course you will understand the requirement for:
                <ul class="mrgleft5">
                    <li>Leadership</li>
                    <li>Management</li>
                    <li>Social Skills</li>
                    <li>Influencer Marketing</li>
                </ul> 
            </span> -->
            <?php
            //Nihar to show teacher
            $userroles = enrol_get_course_users_roles($id);
            $roleid = $DB->get_record('role', array('shortname' => 'editingteacher'))->id;

            ?>
            <h3 class="coursecurriculum commonheader">
                About Instructor                            
            </h3>
            <span>
                <div class="cs_row_four">
                    <?php foreach ($userroles as $key => $userrole) { 
                        $dataobj = $userrole[$roleid];
                        if ($dataobj->roleid == $roleid) {
                        $teacher = $DB->get_record('user', array('id' => $userrole[$roleid]->userid));
                        $teacherlink = html_writer::link($CFG->wwwroot . '/user/profile.php?id=' . $teacher->id, fullname($teacher));
                
                    ?>
                    <div class="about_ins_container">
                        <div class="about_ins_info">
                            <div class="thumb"><img src="http://13.229.224.27/pluginfile.php/164/block_cocoon_course_instructor/content/6.png" alt="6.png"></div>
                        </div>
                        <div class="details">
                            <ul class="about_info_list">
                                <!--<li class="list-inline-item"><span class="flaticon-play-button-1"></span> 5 Courses </li>-->
                            </ul>
                            <h4><?php echo $teacherlink ?></h4>
                            <!--<p class="subtitle">UX/UI Designer</p>-->
                        </div>
                    </div>
                    <?php } }?>
                </div>
            </span>

            
        </div>
        <div class="col-md-6 pad0A">
            <div class="startlearning">
                <?php if (isloggedin()) {?>
                    <a href="<?php echo $url ?>">
                        <button type="button" name="startlearning" id="startlearning">
                            Start Learning
                        </button>
                    </a>
                <?php } else {?>
                    <a href="<?php echo $url ?>">
                        <button type="button" name="startlearning" id="startlearning">
                            Buy Now
                        </button>
                    </a>
                <?php }?>
            </div>

            <br>
            <div class="container-fluid">
                <div class="panel panel-default">
                    <div class="panel-heading courseglanceheader">Course at a Glance</div>
                    <div class="panel-body courseglancebody">
                        <div class="row">
                            <div class="col-md-6 pad10A">
                                <img src="<?php echo $CFG->wwwroot.'/my/pix/calender-img.png'?>" />
                                Duration
                            </div>
                            <div class="col-md-6 pad10A redfont">
                                1 Year
                            </div>
                        </div>
                        <div class="row feerow">
                            <div class="col-md-6 pad10A">
                                <img src="<?php echo $CFG->wwwroot.'/my/pix/feestructure.png'?>" />
                                
                                Course Fees
                            </div>
                            <div class="col-md-6 pad10A redfont">
                                <?php   
                                $data = $DB->get_record_sql("SELECT * FROM {enrol} WHERE courseid= $id AND cost IS NOT NULL");
                                echo $data->currency.' '.$data->cost;
                                ?>
                            </div>
                        </div>
                        <div class="row certificationrow">
                            <div class="col-md-6 pad10A">
                                <img src="<?php echo $CFG->wwwroot.'/my/pix/certification.png'?>" />
                                Certification
                            </div>
                            <div class="col-md-6 pad10A redfont">
                                Online
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
            <h3 class="coursematerial commonheader">Course Material</h3><br/>
            
            <?php
            $modinfo = get_fast_modinfo($id);
            foreach($modinfo->cms as $cm) {
            // Exclude activities that aren't visible or have no view link (e.g. label). Account for folder being displayed inline.
            if (!$cm->uservisible || (!$cm->has_view() && strcmp($cm->modname, 'folder') !== 0)) {
                continue;
            }
            if (array_key_exists($cm->modname, $modfullnames)) {
                continue;
            }
            if (!array_key_exists($cm->modname, $archetypes)) {
                $archetypes[$cm->modname] = plugin_supports('mod', $cm->modname, FEATURE_MOD_ARCHETYPE, MOD_ARCHETYPE_OTHER);
            }
            if ($archetypes[$cm->modname] == MOD_ARCHETYPE_RESOURCE) {
                if (!array_key_exists('resources', $modfullnames)) {
                    $modfullnames['resources'] = get_string('resources');
                }
            } else {
                $modfullnames[$cm->modname] = $cm->modplural;
            }
        }
        if(!empty($modfullnames)){
        core_collator::asort($modfullnames);
        $html = '<div class="row col-md-12">';
        foreach ($modfullnames as $modname => $modfullname) {
            if ($modname === 'resources') {
                $icon = $OUTPUT->pix_icon('icon', '', 'mod_page', array('class' => 'icon'));
                $items = '<a href="'.$CFG->wwwroot.'/course/resources.php?id='.$id.'">'.$icon.$modfullname.'</a>';
                $html .= '<div class="col-md-2 well well-small">'.$items.'</div>';
            } else {
                $icon = $OUTPUT->image_icon('icon', get_string('pluginname', $modname), $modname);
                $items = '<a href="'.$CFG->wwwroot.'/mod/'.$modname.'/index.php?id='.$id.'">'.$icon.$modfullname.'</a>';
                $html .= '<div class="col-md-2 well well-small">'.$items.'</div>';
            }
        }
        }
        $html .= '</div>';
        echo $html;
            ?>
        
    </div>
    
    
</div>


<style>
    body#page-mf-coursedetails {
        color : #000000;
    }
    .curriculumht {
        height: 10rem !important;
    }
    .redfont {
        color : red;
    }
    .roundborder {
        border : 2px solid #34CDCB;
        border-radius: 60%
    }
    .pad0A {
        padding: 0px !important;
    }
    .pad2T {
        padding-top: 2rem !important;
    }
    .commonheader {
        color : #243772 !important;
    }
    .pad10A {
        padding: 10px !important;
    }
    .mrgleft2 {
        margin-left: 2% !important;
        padding : 0px !important;
    }
    .mrgleft5 {
        margin-left: 5% !important;
        padding : 0px !important;
    }
    .colorarrow {
        color : #3433CD !important;
    }
    .hrborderline {
        border-bottom: 1px solid #efefef;
    }
    .rightborderline {
        border-right: 1px solid #efefef;
    }
    .courseglanceheader {
        background-color: #1a2d7d !important;
        height: 3rem;
        color: #FFFFFF !important;
        font-size: 24px;
        padding-left: 2rem;
    }
    .courseglancebody {
        border: 1px solid #213b9d !important;
    }
    .startlearning {
        padding: 1rem;
        display: table;
        margin: 0 auto;
    }
    #startlearning {
        background: linear-gradient(43deg, #2bc6e2, #3b79e0);
        color: #FFFFFF;
        font-size: 2em;
        border-radius: 10px;
        border: 0px solid;
        width: 20rem;
    }
    .notesection {
        border: 1px solid #3433CD !important;
        border-radius: 2rem;
        padding: 13px;
    }
    .font18 {
        font-size: 18px;
    }
    .coursedetailsh1 {
        padding-top: 2rem;
		color:#ed2228;
		text-style:bold;
		height:auto;
    }
    .whoshouldjoin {
        padding-left: 2%;
    }
    .totalhrsrow, .structurerow, .feerow, .certificationrow, #startlearning{
        cursor: pointer;
    }
	
	.subhead {
		font-style:italic;
		color:#0a2cb9;
		 
	}
	.h3 { padding-top:0px!important; }
	.h1 { margin-bottom:0px!important; }
	
    @media screen and (max-width: 600px) and (min-width: 200px) {
    .screenbelow400 {
        display: none;
    }
	 .coursedetailsh1 {
        font-size: 2rem !important;
    }
	
    .screenabove400 {
        display: block;
    }
	.subhead {
		 display: none;
    }
}
@media screen and (max-width: 1024px) and (min-width: 768px) {
    .screenbelow400 {
        display: none;
    }
	 .coursedetailsh1 {
        font-size: 1.8rem !important;
		padding-bottom:0px !important;
    }
	
    .screenabove400 {
        display: block;
    }
	
}
    @media screen and (max-width: 3500px) and (min-width: 601px) {
    .screenabove400 {
        display: none;
    }
    .screenbelow400 {
        display: block;
    }
	.subhead { 
	    font-style:italic;
		color:#0a2cb9;
    }
	
	
}




.cs_row_four .about_ins_container .about_ins_info {
    float: left;
}
.cs_row_four .about_ins_container{
    border-style: solid;
    border-width: 2px;
    border-color: #edeff7;
    border-radius: 5px;
    background-color: rgba(42,53,59,0);
    margin-bottom: 30px;
    padding: 30px;
    position: relative;
    height:12rem;
}
.about_ins_container .about_ins_info .thumb {
    width: 100px;
    height: 100px;
    overflow: hidden;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

.cs_row_four .about_ins_container .details {
    padding-left: 160px;
}
.cs_row_four .about_ins_container .details .about_info_list li {
    font-size: 14px;
    font-family: "Open Sans";
    color: #7e7e7e;
    line-height: 1.2;
    margin-right: 30px;
}

.cs_row_four .about_ins_container .details .subtitle {
    font-size: 14px;
    font-family: "Open Sans";
    color: #898989;
    margin-bottom: 15px;
    line-height: 1.714;
}
.about_ins_container .about_ins_info .thumb img {
    object-fit: cover;
    height: 100%;
    width: 100%;
    max-width: none;
}
</style>