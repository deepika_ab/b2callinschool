<?php

/**
 * Moodle's
 * This file is part of Accubits LMS Product
 *
 * @package   Course details
 * @copyright Accubits
 * @license   This file is copyrighted to Accubits
 */

require_once(dirname(__FILE__) . '/../config.php');

$id = required_param('courseid', PARAM_INT);


// Start setting up the page
$params = array();
$PAGE->set_context(context_system::instance());
$PAGE->set_url('/my/coursedetails.php', $params);
$PAGE->set_pagelayout('base');
$crsdetails = 'Course Details';
$PAGE->set_title($crsdetails);
$PAGE->requires->jquery();
$course = $DB->get_record('course', array('id' => $id));
if(!$course){
    throw new moodle_exception('Course not exist');
}
echo $OUTPUT->header();

global $CFG, $DB, $USER, $OUTPUT;

//Check if the course is confirmed

$coursedata = $DB->get_record('b2c_course_approval', array('courseid' => $id));

if($coursedata->confirmed == 0 && !is_siteadmin()){
    throw new moodle_exception('Course is not confirmed yet from admin');
} else {
    require('template/course_details.php');
}
echo $OUTPUT->footer();