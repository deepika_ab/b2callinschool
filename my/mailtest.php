<?php
$headers  = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=utf-8\r\n";
mail('deepika@accubits.com', 'Test Subject', 'Test Msg',$headers);

$sender = 'deepika@accubits.com';
$recipient = 'deepikapdev@gmail.com';

$subject = "php mail test";
$message = "php test message";
$headers = 'From:' . $sender;

if (mail($recipient, $subject, $message, $headers))
{
    echo "Message accepted";
}
else
{
    echo "Error: Message not accepted";
}
$message = '';
if($_POST) {


    if(empty($_POST['email'])  || 
   empty($_POST['firstname']) ||
   empty($_POST['phone']) || 
   empty($_POST['lastname']))
    {
        $message = " Missing: Fill all required fields";

    } else {
        $email  = $_POST["email"];
        $name   = $_POST["firstname"].' '.$_POST["lastname"];
        $phone = $_POST["phone"];
        $subject = "Contact Form Submission";
        $content = "Hi, </br> Submission Details </br> ";
        $content.= 'Name :'.$name ."</br>";
        $content.= "Email :".$email. "</br>";
        $content.= "Phone :".$phone. "</br>";
$headers  = "MIME-Version: 1.0\r\n";
  $headers .= "Content-type: text/html; charset=utf-8\r\n";
mail('deepika@accubits.com', 'Test Subject', 'Test Msg',$headers);
		
        $toEmail = "deepika@accubits.com";
        $mailHeaders = "From: " . $name . "<". $phone . "<". $email .">\r\n"; 
        if(mail($toEmail, $subject, $content, $mailHeaders)) {
            $message = "Your contact information is received successfully.";
        } else {
            $message = "Sorry! Failed to send contact information.";

        }
    }
	
}
?>

<html>

<head>
    <title>Allschool - Website | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <link href="landing.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
</head>

<body>
    <div class="main___">
    <div class="wrapper">
        <div class="logo">
            <img src="logowhite.png" />
        </div>
        <hr>
        <!-- banner -->
        <div class="banner">
            <img src="4.png" width="50%" />
            <p>All In School is an e-learning marketplace that connects students with private tutors and educational
                institutions around the globe. Students can choose to learn at their own pace with on-demand courses or
                to sign up for live classes for a classroom-like experience.</p>

            <p> <span>Your opportunity:</span> We're looking to collaborate with teachers that are ready to boost their
                career and
                increase their income with All In School.</p>
            <p> We give you the choice to upload recorded courses to be available on our marketplace on demand, or to
                use our online live classroom facility for a face-to-face teaching experience.</p>

            <p> All in School will advertise your courses and classes to students anywhere in the world. You set the
                price and you receive an automatic payment as soon as the students enroll and complete the payment. </p>
            </p>
            <div class="list">
                <h4>BENEFITS TO TEACHERS:</h4>
                <ul>
                    <li>-No-subscription fees</li>
                    <li>-Reliable direct remuneration</li>
                    <li>-E-commerce integration and marketing</li>
                    <li>-Interactive live classroom with digital whiteboard</li>
                    <li>-High-definition secure live streaming</li>
                    <li>-Server-side recording</li>
                    <li>-Course building and management tools available</li>
                    <li>-Gamification, quizzes, polls</li>
                    <li>-Discussion and feedback forums</li>
                    <li>-E-learning analytics and reporting tools to promote your courses</li>
                    <li>-API and plugins integration</li>
                    <li>-Technical support</li>
                </ul>
            </div>
        </div>
        <!-- form -->
        
        <div class="form">
            <h4>Expand your teaching horizons. Join All In School!
            </h4>
            <p>Send us your contact details to learn more about an exciting teaching opportunity at All In School.
            </p>
            <?php
                        if (! empty($message)) {
                            ?>
                            <p style="color: #9F6000;background-color: #FEEFB3;" ><?php echo $message; ?></p>
                        <?php
                        }
                        ?>
            <form method="post" action="mailtest.php" enctype="multipart/form-data">
                <div class="input_wrap">
                    <label>Email Address</label>
                    <input type="email" name="email" id="email" required/>
                </div>
                <div class="input_wrap">
                    <label>First Name</label>
                    <input type="text" name="firstname" id="firstname" required/>
                </div>
                <div class="input_wrap">
                    <label>Last Name</label>
                    <input type="text" name="lastname" id="lastname" required/>
                </div>
                <div class="input_wrap">
                    <label>Phone Number</label>
                    <input type="text" name="phone" id="phone" required/>
                </div>
                <input type="submit" class="button" value="Learn More"/>
            </form>
        </div>
        <!-- join school -->
        <div class="join_school">
            <p>Join All In School
            </p>
            <h4>Enjoy an enriching experience
            </h4>
            <div class="flex_wrap">
                <div class="item">
                    <img src="join1.jpg"/>
                    <h4>Lifelong learning
                    </h4>
                    <p>Limitless resources. Stay updated. Help students achieve their goals.

                    </p>
                </div>
                <div class="item">
                    <img src="join2.jpg"/>
                    <h4>Join our community, expand your network</h4>
                    <p>Connect with students and educators from all over the world. Share ideas and get inspired.</p>
                </div>
            </div>
        </div>

    </div>
        <!-- footer -->
        <footer>
            <div class="social">
                <a href="https://www.linkedin.com/company/all-in-school">
                    <img src="ln.png"/>
                </a>
                <a href="http://www.instagram.com/allinschool">
                    <img src="insta.png"/>
                </a>
                <a href="https://allinschool.com/">
                    <img src="color.png"/>
                </a>
                <<a href="mailto:info@allinschool.com">
                    <img src="mail.png"/>
                </a>
            </div>
        </footer>
    </div>
</body>

</html>

