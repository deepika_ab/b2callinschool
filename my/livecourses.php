<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');
require_once($CFG->dirroot.'/course/renderer.php');

redirect_if_major_upgrade_required();

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/livecourses.php', $params);
$PAGE->set_pagelayout('base');
$PAGE->set_pagetype('livecourses');
$PAGE->blocks->add_region('livecourses');
$PAGE->set_title($pagetitle);
$PAGE->set_heading($header);
$PAGE->requires->css(new moodle_url('/my/css/style.css'));
$PAGE->requires->js(new moodle_url('js/jquery.min.js'));
$PAGE->requires->js(new moodle_url('js/custom.js'));

echo $OUTPUT->header();
$catdatas = $DB->get_records_sql("SELECT * FROM {course_categories} WHERE visible = 1");

$url = $CFG->wwwroot.'/my/livecourses.jpg';
$html .= '<div class="row"><div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="main-title text-left" style="padding: 5rem;">
                <h3 class="mt0">
                    Get live lessons with top rated private tutors
                    </br>
                    Learn anything, anywhere, anytime, your way.
                </h3>
                 <div class="btn_wrap">
                        <a class="btn login btn-pink" href="'.$CFG->wwwroot.'/login/signup.php'.'">Join Now</a>
                    </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="main-title text-center">'.html_writer::img($url,'ondemand', array('height' => '400px')).'</div>
        </div>
    </div>
    <hr/>
    <div class="row container-fluid">
        <div class="col-xs-12">
            <div class="row">';

foreach($catdatas as  $catdata){
    //$context = context_coursecat::instance($catid);
    $categorydetail = $DB->get_record('course_categories', array('id'=>$catdata->id), '*', MUST_EXIST);
    //$content = file_rewrite_pluginfile_urls($categorydetail->description, 'pluginfile.php', $context->id, 'coursecat', 'description', null);
    $coursecat = core_course_category::get($categorydetail->id);
    $chelper = new coursecat_helper();
    $content = html_writer::div($chelper->get_category_formatted_description($coursecat));

                $html .= '<div class="col-md-4 ">
                    <div class="top_courses list ccnWithFoot">
                        <div class="details">
                            <div class="tc_content">
                                <p class="font-size-16">'.strtoupper($catdata->name).'</p>
                                <a href="'.$CFG->wwwroot.'/course/index.php?categoryid='.$catdata->id.'">'
                                    . '<h5>'.$content.'</h5>
                                </a>
                            </div>
                        </div>
                        <div class="tc_footer">
                            <ul class="tc_meta float-left">
                                <li class="list-inline-item"><a href="' . $CFG->wwwroot . '/my/teacherlist.php?catid=' . $catdata->id . '"><button class="btn btn-pink">Click to view</button></a></li>
                                <li class="list-inline-item">'.$enrol.'</li>
                            </ul>
                        </div>
                    </div>
                </div><div class="clearfix"></div>';
                }

                
            $html .= '</div>
        </div></div>
    </div>
</div>';
echo $html;
echo $OUTPUT->footer();
?>
<style>
.top_courses.ccnWithFoot:not(.list) {
    padding-bottom: 80px;
}
.top_courses:not(.list) {
    height: calc(100% - 30px);
    padding-bottom: 50px;
    margin-bottom: 0;
}
.top_courses {
    background-color: #fff;
    border: 2px solid #eee;
    border-radius: 5px;
    margin-bottom: 30px;
    overflow: hidden;
    position: relative;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}
.top_courses .details {
    position: relative;
}
.top_courses .details .tc_content {
    padding: 20px;
}
.top_courses:not(.list) .tc_footer {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    height: 50px;
}
.top_courses .tc_footer {
    /*border-top: 1px solid #eee;*/
    display: flow-root;
    /*padding: 10px 20px;
    position: relative;*/
    position: absolute;
    bottom: 2%;
    left: 33%;
}
.top_courses .tc_footer .tc_meta {
    margin-bottom: 0;
    padding: 0px !important;
}
.float-left {
    float: left!important;
}
.top_courses .tc_footer .tc_meta li {
    margin-right: 4px;
}
.list-inline-item:not(:last-child) {
    margin-right: .5rem;
}
.list-inline-item {
    display: inline-block;
}
.btn-transparent:hover, .btn-transparent:active, .btn-transparent:focus {
    background-color: #2441e7;
    color: #fff;
    border: 2px solid;
    border-color: #2441e7;
}
.courses_all_btn .btn {
    border: 2px solid;
    border-radius: 25px;
    font-size: 15px;
    height: 50px;
    line-height: 47px;
    margin-top: 30px;
    padding: 0 60px;
}
#cat-button {
    background-size: cover;
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 30px;
    height: 170px;
    width: 100%;
    display: table;
    position: relative;
    text-align: center;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}
.top_courses img{height: 30% ;}
.top_courses .blured img{opacity:0.3;}
.main-title img{width:100%;}
</style>
<script>
require(['jquery'], function($) {
	$(document).ready(function(){
		$(".tc_content h5").hover(function () {
			$('.tc_content h5').removeClass('blured');
			$(this).addClass('blured');
		});
	});
});
</script>
