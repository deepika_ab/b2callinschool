<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');

redirect_if_major_upgrade_required();

// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/ondemand.php', $params);
$PAGE->set_pagelayout('base');
$PAGE->set_pagetype('ondemand');
$PAGE->blocks->add_region('content');
$PAGE->set_title($pagetitle);
$PAGE->set_heading($header);
$PAGE->requires->css(new moodle_url('/my/css/style.css'));
$PAGE->requires->js(new moodle_url('js/jquery.min.js'));
$PAGE->requires->js(new moodle_url('js/custom.js'));

echo $OUTPUT->header();
$coursedatas = $DB->get_records_sql("SELECT * FROM {course} WHERE visible = 1 AND id != 1");
foreach($coursedatas as $coursedata){
    $coursecontext = context_course::instance($coursedata->id);
    $key = $coursedata->id.'::'.$coursedata->fullname;
    $enrolcount[$key] = count_enrolled_users($coursecontext);

}
arsort($enrolcount);
$enrolcount = array_slice($enrolcount, 0, 6);

$url = $CFG->wwwroot.'/my/ondemand.jpg';
echo html_writer::img($url,'ondemand', array('width'=> '100%', ));
echo '<br/>';
$html .= '<div class="row"><div class="container" id="yui_3_17_2_1_1602914185870_21">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <div class="main-title text-center"><h3 class="pt-50 pb-50">Browse Our Top Courses</h3>
            </div>
        </div>
    </div>
    <div class="row" id="yui_3_17_2_1_1602914185870_20">
        <div class="col-xs-12">
            <div class="row">';

foreach($enrolcount as $key => $enrol){
$keyexplode = explode('::', $key);

                $html .= '<div class="col-md-4 ">
                    <div class="top_courses list ccnWithFoot">
                        <div class="details">
                            <div class="tc_content">
                                <a href="'.$CFG->wwwroot.'/course/view.php?id='.$keyexplode[0].'"><h5>'.$keyexplode[1].'</h5></a>
                            </div>
                        </div>
                        <div class="tc_footer">
                            <ul class="tc_meta float-left">
                                <li class="list-inline-item">
                                    <i class="fa fa-user"></i>
                                </li>
                                <li class="list-inline-item">'.$enrol.'</li>
                            </ul>
                        </div>
                    </div>
                </div>';
                }

                
            $html .= '</div>
        </div></div>
        <div class="col-lg-6 offset-lg-3">
            <div class="courses_all_btn text-center">
                <a class="btn btn-transparent" href="/course/index.php?categoryid=1">View all courses</a>
            </div>
        </div>
    </div>
</div>';
echo $html;
echo '<br/>';
$html1 .= '<div class="row">
    <div class="col-lg-6 offset-lg-3">
            <div class="main-title text-center">
                    <h3 data-ccn="title" class="pt-50 pb-50" data-ccn-c="color_title" data-ccn-co="content" style="color:#0a0a0a;">Browse Our Course Categories</h3>
            </div>
    </div>
</div>';
$categories = $DB->get_records('course_categories');
foreach ($categories as $category) {
	if ($category->visible == 1) {
		$visible_categories[]= $category;
	}
}
$allcatcount = count($visible_categories); 
$catlimit = 9;
$w =0;

foreach ($visible_categories as $category) {
	if($w<$catlimit){
	$categoryname = $category->name;
	$cat .= '<div class="col-md-4"><button type="button" id="cat-button" class="btn btn-primary btn-lg"><a style="color:#FFFFFF" href="' . $CFG->wwwroot . '/course/index.php?categoryid=' . $category->id . '">' . $categoryname . ' <span>(' . $category->coursecount . ')</span></a></button></div>';
	$w++; }
}

if($w<$allcatcount){
	        $cat .='<div class="col-lg-6 offset-lg-3">
            <div class="courses_all_btn text-center">
                <a class="btn btn-transparent load-more" href="#" id="load_'.$w.'">Load More</a>
            </div>
            <input type="hidden" id="all" value="'.$allcatcount.'">
        </div>';
}

$html1 .= '<div class="row l-block-8 l-container">
<div class="col-md-12 row course_cat">'.$cat.'</div>
</div>';
echo $html1;
echo $OUTPUT->footer();
?>
<style>
.top_courses.ccnWithFoot:not(.list) {
    padding-bottom: 80px;
}
.top_courses:not(.list) {
    height: calc(100% - 30px);
    padding-bottom: 50px;
    margin-bottom: 0;
}
.top_courses {
    background-color: #fff;
    border: 2px solid #eee;
    border-radius: 5px;
    margin-bottom: 30px;
    overflow: hidden;
    position: relative;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}
.top_courses .details {
    position: relative;
}
.top_courses .details .tc_content {
    padding: 20px;
}
.top_courses:not(.list) .tc_footer {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    height: 50px;
}
.top_courses .tc_footer {
    border-top: 1px solid #eee;
    display: flow-root;
    padding: 10px 20px;
    position: relative;
}
.top_courses .tc_footer .tc_meta {
    margin-bottom: 0;
    padding: 0px !important;
}
.float-left {
    float: left!important;
}
.top_courses .tc_footer .tc_meta li {
    margin-right: 4px;
}
.list-inline-item:not(:last-child) {
    margin-right: .5rem;
}
.list-inline-item {
    display: inline-block;
}
.btn-transparent:hover, .btn-transparent:active, .btn-transparent:focus {
    background-color: #2441e7;
    color: #fff;
    border: 2px solid;
    border-color: #2441e7;
}
.courses_all_btn .btn {
    border: 2px solid;
    border-radius: 25px;
    font-size: 15px;
    height: 50px;
    line-height: 47px;
    margin-top: 30px;
    padding: 0 60px;
}
#cat-button {
    background-size: cover;
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 30px;
    height: 170px;
    width: 100%;
    display: table;
    position: relative;
    text-align: center;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}

</style>
<script>
require(['jquery'], function($) {
	$(document).ready(function(){
		// Load more data
		$(document).on('click', '.load-more', function(e) {
			e.preventDefault();
			var total_cat = Number($('#all').val());
			var lastrow = $(this).attr('id');
			$.ajax({
				url: 'getCategories.php',
				type: 'post',
				data: {row:lastrow,total_cat:total_cat},
				beforeSend:function(){
					$(".load-more").text("Loading...");
				},
				success: function(response){
					$('.course_cat').append(response);
					//remove old load more button
					$('#'+lastrow).remove();
				}
			});

		});

	});
});
</script>
