<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../config.php');

redirect_if_major_upgrade_required();

$id = optional_param('catid', null, PARAM_INT);
// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/teacherlist.php', $params);
$PAGE->set_pagelayout('base');
$PAGE->set_pagetype('Teacherlist');
$PAGE->blocks->add_region('Teacherlist');
$PAGE->set_title('Teacherlist');
$PAGE->set_heading('Teacherlist');
$PAGE->requires->jquery();

echo $OUTPUT->header();

$courses = $DB->get_records('course', array('category' => $id));

$table = new html_table();
$table->head  = array('Slno','Teacher', 'Assigned Course');
$table->id = 'teacherlist';
$roleid = $DB->get_record('role', array('shortname' => 'editingteacher'))->id;
$i =1;
$html = '';
foreach ($courses as $coursedata) {
    $userroles = enrol_get_course_users_roles($coursedata->id);
    foreach ($userroles as $key => $userrole) {
        $dataobj = $userrole[$roleid];
        if ($dataobj->roleid == $roleid) {
            $teacherarr[] = $userrole[$roleid]->userid;
            $teacher = $DB->get_record('user', array('id' => $userrole[$roleid]->userid));
            $userpicture = $OUTPUT->user_picture($teacher, array('size'=> '100'));
            //$table->data[] = array($i, $userpicture . ' ' . fullname($teacher), $coursedata->fullname);
            //$i++;
            $html .='<div style="border:1px solid #e2e0e0;" class="course-summaryitem m-b-1 p-2 wdm-course-summary" role="listitem" data-region="course-content" data-course-id="3">
                        <div class="d-flex">'.$userpicture.'
                            <div class="align-self-stretch d-flex flex-column w-p100">
                                <div class="mb-1">
                                    <a href="'.$CFG->wwwroot.'/user/profile.php?id='.$teacher->id.'" class="aalink coursename">
                                        <h3 class="d-inline">'.fullname($teacher).'</h3>
                                    </a>
                                </div>
                                <div class="summary">
                                    <p dir="ltr" style="text-align:left;">'.$teacher->description.'</p>
                                </div>
                            </div>
                        </div>
                    </div>';
        }
    }
}

echo $html;
echo $OUTPUT->footer();
?>

<style>

    
</style>