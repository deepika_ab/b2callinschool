<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
require_once(__DIR__ . '/../config.php');

$allcatcount = $_POST['total_cat'];
$last_row = str_replace('load_', '', $_POST['row']);
$catlimit = 9;

$sql = "SELECT * FROM {course_categories} WHERE visible = 1 LIMIT ".$last_row.",".$catlimit."";
$categories = $DB->get_records_sql($sql);
$w= 0;
foreach ($categories as $category) {
	if($w<$catlimit){
	$categoryname = $category->name;
	$cat .= '<div class="col-md-4"><button type="button" id="cat-button" class="btn btn-primary btn-lg"><a style="color:#FFFFFF" href="' . $CFG->wwwroot . '/course/index.php?categoryid=' . $category->id . '">' . $categoryname . ' <span>(' . $category->coursecount . ')</span></a></button></div>';
	$w++; }
}
$newlast_row = $last_row+$w;
if($newlast_row<$allcatcount){
	$cat .='<div class="col-lg-6 offset-lg-3">
	<div class="courses_all_btn text-center">
		<a class="btn btn-transparent load-more" href="#" id="load_'.$newlast_row.'">Load More</a>
	</div>
	<input type="hidden" id="all" value="'.$allcatcount.'">
	</div>';
}
echo $cat;
?>
