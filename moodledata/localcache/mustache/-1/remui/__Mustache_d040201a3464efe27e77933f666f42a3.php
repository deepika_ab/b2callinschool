<?php

class __Mustache_d040201a3464efe27e77933f666f42a3 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<section data-region="blocks-column" class="d-print-none page-aside" aria-label="';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionC14df02445cdd505a0208e8a56a5f32e($context, $indent, $value);
        $buffer .= '">
';
        // 'initrightsidebar' section
        $value = $context->find('initrightsidebar');
        $buffer .= $this->section02fd7df13417c5acbc2aefd1a8e9c6a0($context, $indent, $value);
        $buffer .= $indent . '</section>
';
        // 'js' section
        $value = $context->find('js');
        $buffer .= $this->section94fe61eba3a1b4338c61641df9a857f6($context, $indent, $value);

        return $buffer;
    }

    private function sectionC14df02445cdd505a0208e8a56a5f32e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'blocks';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'blocks';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section55929a8501822de4d9ef1f82d6913642(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <span class="unread-count-sidebarbutton font-size-10 count-container animation-shake position-absolute" style="top: 0px;left: -5px;">{{unreadrequestcount}}</span>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <span class="unread-count-sidebarbutton font-size-10 count-container animation-shake position-absolute" style="top: 0px;left: -5px;">';
                $value = $this->resolveValue($context->find('unreadrequestcount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</span>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section0ae4b3440846568ec79954f7fc9b4ea0(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'unpinsidebar, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'unpinsidebar, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6c672f2910424e69ca04ee06a3ca5c53(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '{{#str}}unpinsidebar, theme_remui{{/str}}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section0ae4b3440846568ec79954f7fc9b4ea0($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2c94a430a4fc5676a9997c23bcab7f08(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'pinsidebar, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'pinsidebar, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section912f23216d5a286cced7bd426a411ad2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <li class="nav-item text-center">
                <a class="nav-link active show py-3" data-toggle="tab" href="#sidebar-blocks" role="tab" aria-label="Post Side Blocks" aria-selected="true">
                <i class="icon fa fa-th-large" aria-hidden="true"></i>
                </a>
            </li>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <li class="nav-item text-center">
';
                $buffer .= $indent . '                <a class="nav-link active show py-3" data-toggle="tab" href="#sidebar-blocks" role="tab" aria-label="Post Side Blocks" aria-selected="true">
';
                $buffer .= $indent . '                <i class="icon fa fa-th-large" aria-hidden="true"></i>
';
                $buffer .= $indent . '                </a>
';
                $buffer .= $indent . '            </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section625c7e1bbbd5e24b00bfd715a783550c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    {{^messagetoggle}}
                        aria-selected="false"
                    {{/messagetoggle}}
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'messagetoggle' inverted section
                $value = $context->find('messagetoggle');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                        aria-selected="false"
';
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section08fe769975cb72de0644995dd3b55892(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    aria-selected="false"
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    aria-selected="false"
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE1c53482e32bf74d15976d668957db61(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <li class="nav-item text-center">
                <a class="nav-link py-3 {{^hasblocks}}{{^messagetoggle}}active show{{/messagetoggle}}{{/hasblocks}}" data-toggle="tab" href="#sidebar-settings" role="tab" 
                {{^hasblocks}}
                    {{^messagetoggle}}
                        aria-selected="true"
                    {{/messagetoggle}}
                {{/hasblocks}}
                {{#hasblocks}}
                    {{^messagetoggle}}
                        aria-selected="false"
                    {{/messagetoggle}}
                {{/hasblocks}}
                {{^hasblocks}}
                    {{#messagetoggle}}
                    aria-selected="false"
                    {{/messagetoggle}}
                {{/hasblocks}}

                aria-label="Theme Color Customization"

                >
                <i class="icon fa fa-paint-brush" aria-hidden="true"></i>
                </a>
            </li>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <li class="nav-item text-center">
';
                $buffer .= $indent . '                <a class="nav-link py-3 ';
                // 'hasblocks' inverted section
                $value = $context->find('hasblocks');
                if (empty($value)) {
                    
                    // 'messagetoggle' inverted section
                    $value = $context->find('messagetoggle');
                    if (empty($value)) {
                        
                        $buffer .= 'active show';
                    }
                }
                $buffer .= '" data-toggle="tab" href="#sidebar-settings" role="tab" 
';
                // 'hasblocks' inverted section
                $value = $context->find('hasblocks');
                if (empty($value)) {
                    
                    // 'messagetoggle' inverted section
                    $value = $context->find('messagetoggle');
                    if (empty($value)) {
                        
                        $buffer .= $indent . '                        aria-selected="true"
';
                    }
                }
                // 'hasblocks' section
                $value = $context->find('hasblocks');
                $buffer .= $this->section625c7e1bbbd5e24b00bfd715a783550c($context, $indent, $value);
                // 'hasblocks' inverted section
                $value = $context->find('hasblocks');
                if (empty($value)) {
                    
                    // 'messagetoggle' section
                    $value = $context->find('messagetoggle');
                    $buffer .= $this->section08fe769975cb72de0644995dd3b55892($context, $indent, $value);
                }
                $buffer .= $indent . '
';
                $buffer .= $indent . '                aria-label="Theme Color Customization"
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                >
';
                $buffer .= $indent . '                <i class="icon fa fa-paint-brush" aria-hidden="true"></i>
';
                $buffer .= $indent . '                </a>
';
                $buffer .= $indent . '            </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionFc2ca66bb33b3c3233f65a673da1eb08(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="tab-pane fade p-0 active show" id="sidebar-blocks">
                {{{ sidepreblocks }}}
            </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="tab-pane fade p-0 active show" id="sidebar-blocks">
';
                $buffer .= $indent . '                ';
                $value = $this->resolveValue($context->find('sidepreblocks'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7baa287cffccde20a70b82432517b5ea(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="tab-pane fade p-0 {{^hasblocks}}active show{{/hasblocks}}" id="sidebar-message">
                {{{ messagedrawer }}}
            </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="tab-pane fade p-0 ';
                // 'hasblocks' inverted section
                $value = $context->find('hasblocks');
                if (empty($value)) {
                    
                    $buffer .= 'active show';
                }
                $buffer .= '" id="sidebar-message">
';
                $buffer .= $indent . '                ';
                $value = $this->resolveValue($context->find('messagedrawer'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4d9af92342adfe03fae0d5d290a98a7f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' navbartype, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' navbartype, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7749362ecb5f8c27fe0f43d5e969d66c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' checked ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' checked ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEb0b000ca9923c7ed9a39e8366d8d417(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' applysitecolor, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' applysitecolor, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6f6c91b1774a3f1ee719b10ced53a143(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' sidebarcolor, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' sidebarcolor, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section71c11fa3c2c7dd7c3ca1eb370e0d6080(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' dark, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' dark, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section67628447a4a25e3448628902effe6c1b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' light, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' light, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5cecd35560b4f3b9aa53ba4f4b78e9cb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' sitecolor, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' sitecolor, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section182551993ca6d53cbca630fb85efe081(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' primary, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' primary, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section38aebabb456a1b4155a7a66dd597dbaf(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' brown, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' brown, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9354c0b608913db2d55dc3855873ff24(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' cyan, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' cyan, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1663d779a4541f3f25de6a244f9a36ac(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' green, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' green, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC3247964f401e1bba0d9e06d3be88204(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' grey, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' grey, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8e51acf13c2a8d0407b6016f172b2cc7(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' indigo, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' indigo, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8d1b1ce0b7f4d5cadf47902fb42a1656(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' orange, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' orange, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section77f6f6fc169a3eeee5c4d1a9640b4f54(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' pink, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' pink, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section99edce6ab55092a45e328646f8c4cb31(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' purple, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' purple, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4c0c6d1cdaef885ccc1f92f31262b0ad(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' red, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' red, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1f9a49ee41426a8614db10d5f6334c64(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' teal, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' teal, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionDd5edcc324607d16deba6ddf535d6187(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' custom-color, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' custom-color, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionDc36390531be9ac18b17845d1ba5803f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' choose ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' choose ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section48889b9f3f273ba8c7c463afc8a04b66(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' cancel ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' cancel ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section79dfac18c5cac5673af0e9c75fe27f6f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '#{{ sitecolorhex }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '#';
                $value = $this->resolveValue($context->find('sitecolorhex'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section3392c4b9269d96a1a4379ccef49fc210(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' applysitewide, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' applysitewide, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEc5a2d85ed8a52fe37cf7babd10128ca(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="tab-pane fade p-0 {{^hasblocks}}{{^messagetoggle}}active show{{/messagetoggle}}{{/hasblocks}}" id="sidebar-settings">
                <div class="site-skintools px-4 py-2">
                    <!-- navbr type -->
                    <h5 class="site-skintools-title">{{#str}} navbartype, theme_remui {{/str}}</h5>
                    <div id="skintoolsNavbar">
                        <div class="checkbox-custom checkbox-inverse">
                            <input id="skintoolsNavbar-inverse" name="navbartype" type="checkbox" value="navbar-inverse" {{# navbarinverse }} checked {{/ navbarinverse }}>
                            <label for="skintoolsNavbar-inverse">{{#str}} applysitecolor, theme_remui {{/str}}</label>
                        </div>
                    </div>

                    <!-- sidebar color -->
                    <div id="skintoolsSidebar">
                        <h5 class="site-skintools-title">{{#str}} sidebarcolor, theme_remui {{/str}}</h5>
                        <div class="radio-custom radio-dark">
                            <input id="skintoolsSidebar-dark" type="radio" name="skintoolsSidebar" value="" checked>
                            <label for="skintoolsSidebar-dark">{{# str }} dark, theme_remui {{/ str }}</label>
                        </div>
                        <div class="radio-custom radio-light">
                            <input id="skintoolsSidebar-light" type="radio" name="skintoolsSidebar" value="site-menubar-light" {{# sidebarcolor }} checked {{/sidebarcolor }}>
                            <label for="skintoolsSidebar-light">{{# str }} light, theme_remui {{/ str }}</label>
                        </div>
                    </div>

                    <!-- site color -->
                    <div id="skintoolsSiteColor">
                        <h5 class="site-skintools-title">{{#str}} sitecolor, theme_remui {{/str}}</h5>
                        <div class="radio-custom radio-primary">
                            <input id="skintoolsNavbar-primary" name="skintoolsNavbar" type="radio" value="primary" {{# primary }} checked {{/ primary }}>
                            <label for="skintoolsNavbar-primary">{{# str }} primary, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-brown">
                            <input id="skintoolsNavbar-brown" name="skintoolsNavbar" type="radio" value="brown" {{# brown }} checked {{/ brown }}>
                            <label for="skintoolsNavbar-brown">{{# str }} brown, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-cyan">
                            <input id="skintoolsNavbar-cyan" name="skintoolsNavbar" type="radio" value="cyan" {{# cyan }} checked {{/ cyan }}>
                            <label for="skintoolsNavbar-cyan">{{# str }} cyan, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-green">
                            <input id="skintoolsNavbar-green" name="skintoolsNavbar" type="radio" value="green" {{# green }} checked {{/ green }}>
                            <label for="skintoolsNavbar-green">{{# str }} green, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-grey">
                            <input id="skintoolsNavbar-grey" name="skintoolsNavbar" type="radio" value="grey" {{# grey }} checked {{/ grey }}>
                            <label for="skintoolsNavbar-grey">{{# str }} grey, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-indigo">
                            <input id="skintoolsNavbar-indigo" name="skintoolsNavbar" type="radio" value="indigo" {{# indigo }} checked {{/ indigo }}>
                            <label for="skintoolsNavbar-indigo">{{# str }} indigo, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-orange">
                            <input id="skintoolsNavbar-orange" name="skintoolsNavbar" type="radio" value="orange" {{# orange }} checked {{/ orange }}>
                            <label for="skintoolsNavbar-orange">{{# str }} orange, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-pink">
                            <input id="skintoolsNavbar-pink" name="skintoolsNavbar" type="radio" value="pink" {{# pink }} checked {{/ pink }}>
                            <label for="skintoolsNavbar-pink">{{# str }} pink, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-purple">
                            <input id="skintoolsNavbar-purple" name="skintoolsNavbar" type="radio" value="purple" {{# purple }} checked {{/ purple }}>
                            <label for="skintoolsNavbar-purple">{{# str }} purple, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-red">
                            <input id="skintoolsNavbar-red" name="skintoolsNavbar" type="radio" value="red" {{# red }} checked {{/ red }}>
                            <label for="skintoolsNavbar-red">{{# str }} red, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-teal">
                            <input id="skintoolsNavbar-teal" name="skintoolsNavbar" type="radio" value="teal" {{# teal }} checked {{/ teal }}>
                            <label for="skintoolsNavbar-teal">{{# str }} teal, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-customcolor">
                            <input id="skintoolsNavbar-customcolor" name="skintoolsNavbar" type="radio" value="customcolor" {{# customcolor }} checked {{/ customcolor }}>
                            <label for="skintoolsNavbar-customcolor">{{# str }} custom-color, theme_remui {{/ str}}</label>
                        </div>
                    </div>

                    <!-- site color picker-->
                    <form method="post" class="text-center">
                        <input type="color" id="{{uniqid}}" data-choosetext="{{# str }} choose {{/ str }}" data-canceltext="{{# str }} cancel {{/ str }}" name="customcolor" class="site-colorpicker d-none" hidden style="display:none;" value="{{^ customcolor }}#62a8ea{{/ customcolor }}{{# customcolor }}#{{ sitecolorhex }}{{/ customcolor }}">
                        <input type="hidden" name="choosen-color">
                        <input type="hidden" name="sesskey" value="{{ sesskey }}">
                        <input type="submit" name="applysitewidecolor" class="btn btn-primary my-3" value="{{# str }} applysitewide, theme_remui {{/ str }}">
                    </form>
                </div>
            </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="tab-pane fade p-0 ';
                // 'hasblocks' inverted section
                $value = $context->find('hasblocks');
                if (empty($value)) {
                    
                    // 'messagetoggle' inverted section
                    $value = $context->find('messagetoggle');
                    if (empty($value)) {
                        
                        $buffer .= 'active show';
                    }
                }
                $buffer .= '" id="sidebar-settings">
';
                $buffer .= $indent . '                <div class="site-skintools px-4 py-2">
';
                $buffer .= $indent . '                    <!-- navbr type -->
';
                $buffer .= $indent . '                    <h5 class="site-skintools-title">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section4d9af92342adfe03fae0d5d290a98a7f($context, $indent, $value);
                $buffer .= '</h5>
';
                $buffer .= $indent . '                    <div id="skintoolsNavbar">
';
                $buffer .= $indent . '                        <div class="checkbox-custom checkbox-inverse">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-inverse" name="navbartype" type="checkbox" value="navbar-inverse" ';
                // 'navbarinverse' section
                $value = $context->find('navbarinverse');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-inverse">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionEb0b000ca9923c7ed9a39e8366d8d417($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                    </div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                    <!-- sidebar color -->
';
                $buffer .= $indent . '                    <div id="skintoolsSidebar">
';
                $buffer .= $indent . '                        <h5 class="site-skintools-title">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section6f6c91b1774a3f1ee719b10ced53a143($context, $indent, $value);
                $buffer .= '</h5>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-dark">
';
                $buffer .= $indent . '                            <input id="skintoolsSidebar-dark" type="radio" name="skintoolsSidebar" value="" checked>
';
                $buffer .= $indent . '                            <label for="skintoolsSidebar-dark">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section71c11fa3c2c7dd7c3ca1eb370e0d6080($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-light">
';
                $buffer .= $indent . '                            <input id="skintoolsSidebar-light" type="radio" name="skintoolsSidebar" value="site-menubar-light" ';
                // 'sidebarcolor' section
                $value = $context->find('sidebarcolor');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsSidebar-light">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section67628447a4a25e3448628902effe6c1b($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                    </div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                    <!-- site color -->
';
                $buffer .= $indent . '                    <div id="skintoolsSiteColor">
';
                $buffer .= $indent . '                        <h5 class="site-skintools-title">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section5cecd35560b4f3b9aa53ba4f4b78e9cb($context, $indent, $value);
                $buffer .= '</h5>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-primary">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-primary" name="skintoolsNavbar" type="radio" value="primary" ';
                // 'primary' section
                $value = $context->find('primary');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-primary">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section182551993ca6d53cbca630fb85efe081($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-brown">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-brown" name="skintoolsNavbar" type="radio" value="brown" ';
                // 'brown' section
                $value = $context->find('brown');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-brown">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section38aebabb456a1b4155a7a66dd597dbaf($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-cyan">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-cyan" name="skintoolsNavbar" type="radio" value="cyan" ';
                // 'cyan' section
                $value = $context->find('cyan');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-cyan">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section9354c0b608913db2d55dc3855873ff24($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-green">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-green" name="skintoolsNavbar" type="radio" value="green" ';
                // 'green' section
                $value = $context->find('green');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-green">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section1663d779a4541f3f25de6a244f9a36ac($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-grey">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-grey" name="skintoolsNavbar" type="radio" value="grey" ';
                // 'grey' section
                $value = $context->find('grey');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-grey">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionC3247964f401e1bba0d9e06d3be88204($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-indigo">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-indigo" name="skintoolsNavbar" type="radio" value="indigo" ';
                // 'indigo' section
                $value = $context->find('indigo');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-indigo">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section8e51acf13c2a8d0407b6016f172b2cc7($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-orange">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-orange" name="skintoolsNavbar" type="radio" value="orange" ';
                // 'orange' section
                $value = $context->find('orange');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-orange">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section8d1b1ce0b7f4d5cadf47902fb42a1656($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-pink">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-pink" name="skintoolsNavbar" type="radio" value="pink" ';
                // 'pink' section
                $value = $context->find('pink');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-pink">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section77f6f6fc169a3eeee5c4d1a9640b4f54($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-purple">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-purple" name="skintoolsNavbar" type="radio" value="purple" ';
                // 'purple' section
                $value = $context->find('purple');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-purple">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section99edce6ab55092a45e328646f8c4cb31($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-red">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-red" name="skintoolsNavbar" type="radio" value="red" ';
                // 'red' section
                $value = $context->find('red');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-red">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section4c0c6d1cdaef885ccc1f92f31262b0ad($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-teal">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-teal" name="skintoolsNavbar" type="radio" value="teal" ';
                // 'teal' section
                $value = $context->find('teal');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-teal">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section1f9a49ee41426a8614db10d5f6334c64($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                        <div class="radio-custom radio-customcolor">
';
                $buffer .= $indent . '                            <input id="skintoolsNavbar-customcolor" name="skintoolsNavbar" type="radio" value="customcolor" ';
                // 'customcolor' section
                $value = $context->find('customcolor');
                $buffer .= $this->section7749362ecb5f8c27fe0f43d5e969d66c($context, $indent, $value);
                $buffer .= '>
';
                $buffer .= $indent . '                            <label for="skintoolsNavbar-customcolor">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionDd5edcc324607d16deba6ddf535d6187($context, $indent, $value);
                $buffer .= '</label>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                    </div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                    <!-- site color picker-->
';
                $buffer .= $indent . '                    <form method="post" class="text-center">
';
                $buffer .= $indent . '                        <input type="color" id="';
                $value = $this->resolveValue($context->find('uniqid'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" data-choosetext="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionDc36390531be9ac18b17845d1ba5803f($context, $indent, $value);
                $buffer .= '" data-canceltext="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section48889b9f3f273ba8c7c463afc8a04b66($context, $indent, $value);
                $buffer .= '" name="customcolor" class="site-colorpicker d-none" hidden style="display:none;" value="';
                // 'customcolor' inverted section
                $value = $context->find('customcolor');
                if (empty($value)) {
                    
                    $buffer .= '#62a8ea';
                }
                // 'customcolor' section
                $value = $context->find('customcolor');
                $buffer .= $this->section79dfac18c5cac5673af0e9c75fe27f6f($context, $indent, $value);
                $buffer .= '">
';
                $buffer .= $indent . '                        <input type="hidden" name="choosen-color">
';
                $buffer .= $indent . '                        <input type="hidden" name="sesskey" value="';
                $value = $this->resolveValue($context->find('sesskey'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">
';
                $buffer .= $indent . '                        <input type="submit" name="applysitewidecolor" class="btn btn-primary my-3" value="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section3392c4b9269d96a1a4379ccef49fc210($context, $indent, $value);
                $buffer .= '">
';
                $buffer .= $indent . '                    </form>
';
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section02fd7df13417c5acbc2aefd1a8e9c6a0(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <div class="page-aside-switch d-flex align-items-center justify-content-center">
        <i class="fa fa-angle-left icon m-0 d-flex align-items-center justify-content-center" aria-hidden="true">
            {{#unreadrequestcount}}
                <span class="unread-count-sidebarbutton font-size-10 count-container animation-shake position-absolute" style="top: 0px;left: -5px;">{{unreadrequestcount}}</span>
            {{/unreadrequestcount}}
        </i>
        {{^disablesidebarpinning}}<i class="fa fa-thumb-tack icon m-0 d-none align-items-center justify-content-center" title="{{#pinaside}}{{#str}}unpinsidebar, theme_remui{{/str}}{{/pinaside}}{{^pinaside}}{{#str}}pinsidebar, theme_remui{{/str}}{{/pinaside}}" aria-hidden="true"></i>{{/disablesidebarpinning}}
    </div>
    <ul class="site-sidebar-nav nav nav-tabs nav-tabs-line" role="tablist">
        {{#hasblocks}}
            <li class="nav-item text-center">
                <a class="nav-link active show py-3" data-toggle="tab" href="#sidebar-blocks" role="tab" aria-label="Post Side Blocks" aria-selected="true">
                <i class="icon fa fa-th-large" aria-hidden="true"></i>
                </a>
            </li>
        {{/hasblocks}}
        {{{ messagetoggle }}}
        {{#usercanmanage}}
            <li class="nav-item text-center">
                <a class="nav-link py-3 {{^hasblocks}}{{^messagetoggle}}active show{{/messagetoggle}}{{/hasblocks}}" data-toggle="tab" href="#sidebar-settings" role="tab" 
                {{^hasblocks}}
                    {{^messagetoggle}}
                        aria-selected="true"
                    {{/messagetoggle}}
                {{/hasblocks}}
                {{#hasblocks}}
                    {{^messagetoggle}}
                        aria-selected="false"
                    {{/messagetoggle}}
                {{/hasblocks}}
                {{^hasblocks}}
                    {{#messagetoggle}}
                    aria-selected="false"
                    {{/messagetoggle}}
                {{/hasblocks}}

                aria-label="Theme Color Customization"

                >
                <i class="icon fa fa-paint-brush" aria-hidden="true"></i>
                </a>
            </li>
        {{/usercanmanage}}
    </ul>
    <div class="site-sidebar-tab-content tab-content">
        {{#hasblocks}}
            <div class="tab-pane fade p-0 active show" id="sidebar-blocks">
                {{{ sidepreblocks }}}
            </div>
        {{/hasblocks}}
        {{#messagedrawer}}
            <div class="tab-pane fade p-0 {{^hasblocks}}active show{{/hasblocks}}" id="sidebar-message">
                {{{ messagedrawer }}}
            </div>
        {{/messagedrawer}}
        {{#usercanmanage}}
            <div class="tab-pane fade p-0 {{^hasblocks}}{{^messagetoggle}}active show{{/messagetoggle}}{{/hasblocks}}" id="sidebar-settings">
                <div class="site-skintools px-4 py-2">
                    <!-- navbr type -->
                    <h5 class="site-skintools-title">{{#str}} navbartype, theme_remui {{/str}}</h5>
                    <div id="skintoolsNavbar">
                        <div class="checkbox-custom checkbox-inverse">
                            <input id="skintoolsNavbar-inverse" name="navbartype" type="checkbox" value="navbar-inverse" {{# navbarinverse }} checked {{/ navbarinverse }}>
                            <label for="skintoolsNavbar-inverse">{{#str}} applysitecolor, theme_remui {{/str}}</label>
                        </div>
                    </div>

                    <!-- sidebar color -->
                    <div id="skintoolsSidebar">
                        <h5 class="site-skintools-title">{{#str}} sidebarcolor, theme_remui {{/str}}</h5>
                        <div class="radio-custom radio-dark">
                            <input id="skintoolsSidebar-dark" type="radio" name="skintoolsSidebar" value="" checked>
                            <label for="skintoolsSidebar-dark">{{# str }} dark, theme_remui {{/ str }}</label>
                        </div>
                        <div class="radio-custom radio-light">
                            <input id="skintoolsSidebar-light" type="radio" name="skintoolsSidebar" value="site-menubar-light" {{# sidebarcolor }} checked {{/sidebarcolor }}>
                            <label for="skintoolsSidebar-light">{{# str }} light, theme_remui {{/ str }}</label>
                        </div>
                    </div>

                    <!-- site color -->
                    <div id="skintoolsSiteColor">
                        <h5 class="site-skintools-title">{{#str}} sitecolor, theme_remui {{/str}}</h5>
                        <div class="radio-custom radio-primary">
                            <input id="skintoolsNavbar-primary" name="skintoolsNavbar" type="radio" value="primary" {{# primary }} checked {{/ primary }}>
                            <label for="skintoolsNavbar-primary">{{# str }} primary, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-brown">
                            <input id="skintoolsNavbar-brown" name="skintoolsNavbar" type="radio" value="brown" {{# brown }} checked {{/ brown }}>
                            <label for="skintoolsNavbar-brown">{{# str }} brown, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-cyan">
                            <input id="skintoolsNavbar-cyan" name="skintoolsNavbar" type="radio" value="cyan" {{# cyan }} checked {{/ cyan }}>
                            <label for="skintoolsNavbar-cyan">{{# str }} cyan, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-green">
                            <input id="skintoolsNavbar-green" name="skintoolsNavbar" type="radio" value="green" {{# green }} checked {{/ green }}>
                            <label for="skintoolsNavbar-green">{{# str }} green, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-grey">
                            <input id="skintoolsNavbar-grey" name="skintoolsNavbar" type="radio" value="grey" {{# grey }} checked {{/ grey }}>
                            <label for="skintoolsNavbar-grey">{{# str }} grey, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-indigo">
                            <input id="skintoolsNavbar-indigo" name="skintoolsNavbar" type="radio" value="indigo" {{# indigo }} checked {{/ indigo }}>
                            <label for="skintoolsNavbar-indigo">{{# str }} indigo, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-orange">
                            <input id="skintoolsNavbar-orange" name="skintoolsNavbar" type="radio" value="orange" {{# orange }} checked {{/ orange }}>
                            <label for="skintoolsNavbar-orange">{{# str }} orange, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-pink">
                            <input id="skintoolsNavbar-pink" name="skintoolsNavbar" type="radio" value="pink" {{# pink }} checked {{/ pink }}>
                            <label for="skintoolsNavbar-pink">{{# str }} pink, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-purple">
                            <input id="skintoolsNavbar-purple" name="skintoolsNavbar" type="radio" value="purple" {{# purple }} checked {{/ purple }}>
                            <label for="skintoolsNavbar-purple">{{# str }} purple, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-red">
                            <input id="skintoolsNavbar-red" name="skintoolsNavbar" type="radio" value="red" {{# red }} checked {{/ red }}>
                            <label for="skintoolsNavbar-red">{{# str }} red, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-teal">
                            <input id="skintoolsNavbar-teal" name="skintoolsNavbar" type="radio" value="teal" {{# teal }} checked {{/ teal }}>
                            <label for="skintoolsNavbar-teal">{{# str }} teal, theme_remui {{/ str}}</label>
                        </div>
                        <div class="radio-custom radio-customcolor">
                            <input id="skintoolsNavbar-customcolor" name="skintoolsNavbar" type="radio" value="customcolor" {{# customcolor }} checked {{/ customcolor }}>
                            <label for="skintoolsNavbar-customcolor">{{# str }} custom-color, theme_remui {{/ str}}</label>
                        </div>
                    </div>

                    <!-- site color picker-->
                    <form method="post" class="text-center">
                        <input type="color" id="{{uniqid}}" data-choosetext="{{# str }} choose {{/ str }}" data-canceltext="{{# str }} cancel {{/ str }}" name="customcolor" class="site-colorpicker d-none" hidden style="display:none;" value="{{^ customcolor }}#62a8ea{{/ customcolor }}{{# customcolor }}#{{ sitecolorhex }}{{/ customcolor }}">
                        <input type="hidden" name="choosen-color">
                        <input type="hidden" name="sesskey" value="{{ sesskey }}">
                        <input type="submit" name="applysitewidecolor" class="btn btn-primary my-3" value="{{# str }} applysitewide, theme_remui {{/ str }}">
                    </form>
                </div>
            </div>
        {{/usercanmanage}}
    </div>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <div class="page-aside-switch d-flex align-items-center justify-content-center">
';
                $buffer .= $indent . '        <i class="fa fa-angle-left icon m-0 d-flex align-items-center justify-content-center" aria-hidden="true">
';
                // 'unreadrequestcount' section
                $value = $context->find('unreadrequestcount');
                $buffer .= $this->section55929a8501822de4d9ef1f82d6913642($context, $indent, $value);
                $buffer .= $indent . '        </i>
';
                $buffer .= $indent . '        ';
                // 'disablesidebarpinning' inverted section
                $value = $context->find('disablesidebarpinning');
                if (empty($value)) {
                    
                    $buffer .= '<i class="fa fa-thumb-tack icon m-0 d-none align-items-center justify-content-center" title="';
                    // 'pinaside' section
                    $value = $context->find('pinaside');
                    $buffer .= $this->section6c672f2910424e69ca04ee06a3ca5c53($context, $indent, $value);
                    // 'pinaside' inverted section
                    $value = $context->find('pinaside');
                    if (empty($value)) {
                        
                        // 'str' section
                        $value = $context->find('str');
                        $buffer .= $this->section2c94a430a4fc5676a9997c23bcab7f08($context, $indent, $value);
                    }
                    $buffer .= '" aria-hidden="true"></i>';
                }
                $buffer .= '
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '    <ul class="site-sidebar-nav nav nav-tabs nav-tabs-line" role="tablist">
';
                // 'hasblocks' section
                $value = $context->find('hasblocks');
                $buffer .= $this->section912f23216d5a286cced7bd426a411ad2($context, $indent, $value);
                $buffer .= $indent . '        ';
                $value = $this->resolveValue($context->find('messagetoggle'), $context);
                $buffer .= $value;
                $buffer .= '
';
                // 'usercanmanage' section
                $value = $context->find('usercanmanage');
                $buffer .= $this->sectionE1c53482e32bf74d15976d668957db61($context, $indent, $value);
                $buffer .= $indent . '    </ul>
';
                $buffer .= $indent . '    <div class="site-sidebar-tab-content tab-content">
';
                // 'hasblocks' section
                $value = $context->find('hasblocks');
                $buffer .= $this->sectionFc2ca66bb33b3c3233f65a673da1eb08($context, $indent, $value);
                // 'messagedrawer' section
                $value = $context->find('messagedrawer');
                $buffer .= $this->section7baa287cffccde20a70b82432517b5ea($context, $indent, $value);
                // 'usercanmanage' section
                $value = $context->find('usercanmanage');
                $buffer .= $this->sectionEc5a2d85ed8a52fe37cf7babd10128ca($context, $indent, $value);
                $buffer .= $indent . '    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section3b1a65fd65bd9d90190dd0b9f1f24169(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '{{ sitecolorhex }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $value = $this->resolveValue($context->find('sitecolorhex'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section94fe61eba3a1b4338c61641df9a857f6(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
require([\'jquery\', \'theme_remui/color-picker\'], function($) {
    $(\'#{{uniqid}}\').spectrum({
        color: \'#{{^ customcolor }}62a8ea{{/ customcolor }}{{# customcolor }}{{ sitecolorhex }}{{/ customcolor }}\',
        cancelText: $(\'#{{uniqid}}\').data(\'canceltext\'),
        chooseText: $(\'#{{uniqid}}\').data(\'choosetext\'),
        showInput: true,
        preferredFormat: "hex",
        containerClassName: \'border-secondary\',
        replacerClassName: \'{{^ customcolor }} d-none {{/ customcolor }} site-colorpicker-custom w-full bg-white border-0\'
    });
});
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . 'require([\'jquery\', \'theme_remui/color-picker\'], function($) {
';
                $buffer .= $indent . '    $(\'#';
                $value = $this->resolveValue($context->find('uniqid'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '\').spectrum({
';
                $buffer .= $indent . '        color: \'#';
                // 'customcolor' inverted section
                $value = $context->find('customcolor');
                if (empty($value)) {
                    
                    $buffer .= '62a8ea';
                }
                // 'customcolor' section
                $value = $context->find('customcolor');
                $buffer .= $this->section3b1a65fd65bd9d90190dd0b9f1f24169($context, $indent, $value);
                $buffer .= '\',
';
                $buffer .= $indent . '        cancelText: $(\'#';
                $value = $this->resolveValue($context->find('uniqid'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '\').data(\'canceltext\'),
';
                $buffer .= $indent . '        chooseText: $(\'#';
                $value = $this->resolveValue($context->find('uniqid'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '\').data(\'choosetext\'),
';
                $buffer .= $indent . '        showInput: true,
';
                $buffer .= $indent . '        preferredFormat: "hex",
';
                $buffer .= $indent . '        containerClassName: \'border-secondary\',
';
                $buffer .= $indent . '        replacerClassName: \'';
                // 'customcolor' inverted section
                $value = $context->find('customcolor');
                if (empty($value)) {
                    
                    $buffer .= ' d-none ';
                }
                $buffer .= ' site-colorpicker-custom w-full bg-white border-0\'
';
                $buffer .= $indent . '    });
';
                $buffer .= $indent . '});
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
