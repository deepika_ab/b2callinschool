<?php

class __Mustache_67d4cc225b0302d0525f16c73724ffa8 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<div class="container-fluid">
';
        $buffer .= $indent . '    <div class="row justify-content-md-center">
';
        $buffer .= $indent . '        <div class="page-brand-info">
';
        // 'loginpage_context' section
        $value = $context->find('loginpage_context');
        $buffer .= $this->sectionE791ea65c2f05c878176dff88a7199ed($context, $indent, $value);
        // 'sitedesc' section
        $value = $context->find('sitedesc');
        $buffer .= $this->sectionDbd6acb126ba12561646b8d1e2df9ede($context, $indent, $value);
        // 'sitedesc' inverted section
        $value = $context->find('sitedesc');
        if (empty($value)) {
            
            // 'hasinstructions' section
            $value = $context->find('hasinstructions');
            $buffer .= $this->section4761855c690776f6e5e71793de248c23($context, $indent, $value);
        }
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '        <div class="p-0 signupform">
';
        $buffer .= $indent . '            <div class="card">
';
        $buffer .= $indent . '                <div class="card-block p-5">
';
        // 'loginpage_context' section
        $value = $context->find('loginpage_context');
        $buffer .= $this->section97df6aebcbafbc3af353a29bc8123be2($context, $indent, $value);
        $buffer .= $indent . '                    <div class="row col-md-12">
';
        $buffer .= $indent . '						<div class="col-md-6 card-body">
';
        // 'isteacher' section
        $value = $context->find('isteacher');
        $buffer .= $this->section12e1b5bec73262d95b9705de2350bb10($context, $indent, $value);
        // 'isteacher' inverted section
        $value = $context->find('isteacher');
        if (empty($value)) {
            
            $buffer .= $indent . '								<div class="teacher">	
';
            $buffer .= $indent . '									<p>Become<br>
';
            $buffer .= $indent . '									a student process</p>
';
            $buffer .= $indent . '								</div>
';
            $buffer .= $indent . '								<div class="teacher_icon">
';
            $buffer .= $indent . '									<div class="floatLeft">
';
            $buffer .= $indent . '									<img src="https://allinschool.com/my/images/signup-icon.png"/>
';
            $buffer .= $indent . '									</div>
';
            $buffer .= $indent . '							 
';
            $buffer .= $indent . '									<h3 class="teacher_content"><b>Sign Up</b></h3>
';
            $buffer .= $indent . '									<p class="req">We request the students to create an account on <br>Allinschool by filling this Sign Up form.</p>
';
            $buffer .= $indent . '								</div><br>
';
            $buffer .= $indent . '								<div class="teacher_icon">
';
            $buffer .= $indent . '									<img src="https://allinschool.com/my/images/create-teacher-profile.png"/ class="floatLeft">
';
            $buffer .= $indent . '									
';
            $buffer .= $indent . '									<h3 class="teacher_content"><b>Enrol into a course</b></h3>
';
            $buffer .= $indent . '									<p class="req">After signing up, you can purchase courses of your choice. You only need to buy two weeks at a time to get started.</p>
';
            $buffer .= $indent . '								</div><br>
';
            $buffer .= $indent . '								<div class="teacher_icon">
';
            $buffer .= $indent . '									<img src="https://allinschool.com/my/images/teach-courses-icon.png"/ class="floatLeft">
';
            $buffer .= $indent . '									
';
            $buffer .= $indent . '									<h3 class="teacher_content"><b>Learn live from teachers</b></h3>
';
            $buffer .= $indent . '									<p class="req">Learn live from the best teachers online on Allinschool. Use Allinschool’s powerful course studio features to communicate, share and learn from the best.</p>
';
            $buffer .= $indent . '								</div><br>
';
            $buffer .= $indent . '								<div class="teacher_icon">
';
            $buffer .= $indent . '									<img src="https://allinschool.com/my/images/teach-courses-icon.png"/ class="floatLeft">
';
            $buffer .= $indent . '									
';
            $buffer .= $indent . '									<h3 class="teacher_content"><b>Renew with ease</b></h3>
';
            $buffer .= $indent . '									<p class="req">Keep renewing the weeks in your course, just two weeks or as many as you like at a time.</p>
';
            $buffer .= $indent . '								</div><br>
';
        }
        $buffer .= $indent . '						</div>
';
        $buffer .= $indent . '                    
';
        $buffer .= $indent . '						<div class="col-md-6 card-body" style="background-color: #f5f5f5;">
';
        $buffer .= $indent . '							<div class="card-title">
';
        $buffer .= $indent . '								<h3 class="font-size-24">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionAc190bacb4c6349c118f0fe9033b93e9($context, $indent, $value);
        $buffer .= '</h3>
';
        $buffer .= $indent . '							</div>
';
        $buffer .= $indent . '							';
        $value = $this->resolveValue($context->find('formhtml'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '						</div>
';
        $buffer .= $indent . '                </div>
';
        $buffer .= $indent . '                </div>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';
        // 'js' section
        $value = $context->find('js');
        $buffer .= $this->section7d62c0f250d2caa9b153da8ab6cc6e0f($context, $indent, $value);

        return $buffer;
    }

    private function section40b6b29ddc5201098d451565bf8eb193(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    <h2 class="card-header bg-transparent mb-3" ><img src="{{logourl}}" class="img-fluid" title="{{sitename}}" alt="{{sitename}}"/></h2>
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    <h2 class="card-header bg-transparent mb-3" ><img src="';
                $value = $this->resolveValue($context->find('logourl'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" class="img-fluid" title="';
                $value = $this->resolveValue($context->find('sitename'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" alt="';
                $value = $this->resolveValue($context->find('sitename'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"/></h2>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE791ea65c2f05c878176dff88a7199ed(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            {{^logopos}}
                {{#logourl}}
                    <h2 class="card-header bg-transparent mb-3" ><img src="{{logourl}}" class="img-fluid" title="{{sitename}}" alt="{{sitename}}"/></h2>
                {{/logourl}}
                {{^logourl}}
                    <h2 class="card-header bg-transparent mb-3"><i class="fa fa-{{siteicon}}"></i> {{sitename}}</h2>
                {{/logourl}}
            {{/logopos}}
            
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'logopos' inverted section
                $value = $context->find('logopos');
                if (empty($value)) {
                    
                    // 'logourl' section
                    $value = $context->find('logourl');
                    $buffer .= $this->section40b6b29ddc5201098d451565bf8eb193($context, $indent, $value);
                    // 'logourl' inverted section
                    $value = $context->find('logourl');
                    if (empty($value)) {
                        
                        $buffer .= $indent . '                    <h2 class="card-header bg-transparent mb-3"><i class="fa fa-';
                        $value = $this->resolveValue($context->find('siteicon'), $context);
                        $buffer .= call_user_func($this->mustache->getEscape(), $value);
                        $buffer .= '"></i> ';
                        $value = $this->resolveValue($context->find('sitename'), $context);
                        $buffer .= call_user_func($this->mustache->getEscape(), $value);
                        $buffer .= '</h2>
';
                    }
                }
                $buffer .= $indent . '            
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionDbd6acb126ba12561646b8d1e2df9ede(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <p class="font-size-20 site-desc">{{{sitedesc}}}</p>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <p class="font-size-20 site-desc">';
                $value = $this->resolveValue($context->find('sitedesc'), $context);
                $buffer .= $value;
                $buffer .= '</p>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4761855c690776f6e5e71793de248c23(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    <p class="font-size-20 instructions">{{{instructions}}}</p>
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    <p class="font-size-20 instructions">';
                $value = $this->resolveValue($context->find('instructions'), $context);
                $buffer .= $value;
                $buffer .= '</p>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section19307b46bd94bfded9c17abcd815d416(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                            <h2 class="card-header text-center bg-transparent mb-3 p-4" ><img src="{{logourl}}" class="img-fluid" title="{{sitename}}" alt="{{sitename}}"/></h2>
                        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                            <h2 class="card-header text-center bg-transparent mb-3 p-4" ><img src="';
                $value = $this->resolveValue($context->find('logourl'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" class="img-fluid" title="';
                $value = $this->resolveValue($context->find('sitename'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" alt="';
                $value = $this->resolveValue($context->find('sitename'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"/></h2>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC736c129c3a8458939f52c076edf4156(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        {{#logourl}}
                            <h2 class="card-header text-center bg-transparent mb-3 p-4" ><img src="{{logourl}}" class="img-fluid" title="{{sitename}}" alt="{{sitename}}"/></h2>
                        {{/logourl}}
                        {{^logourl}}
                            <h2 class="card-header text-center bg-transparent mb-3 p-4"><i class="fa fa-{{siteicon}}"></i> {{sitename}}</h2>
                        {{/logourl}}
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'logourl' section
                $value = $context->find('logourl');
                $buffer .= $this->section19307b46bd94bfded9c17abcd815d416($context, $indent, $value);
                // 'logourl' inverted section
                $value = $context->find('logourl');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                            <h2 class="card-header text-center bg-transparent mb-3 p-4"><i class="fa fa-';
                    $value = $this->resolveValue($context->find('siteicon'), $context);
                    $buffer .= call_user_func($this->mustache->getEscape(), $value);
                    $buffer .= '"></i> ';
                    $value = $this->resolveValue($context->find('sitename'), $context);
                    $buffer .= call_user_func($this->mustache->getEscape(), $value);
                    $buffer .= '</h2>
';
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section97df6aebcbafbc3af353a29bc8123be2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    {{#logopos}}
                        {{#logourl}}
                            <h2 class="card-header text-center bg-transparent mb-3 p-4" ><img src="{{logourl}}" class="img-fluid" title="{{sitename}}" alt="{{sitename}}"/></h2>
                        {{/logourl}}
                        {{^logourl}}
                            <h2 class="card-header text-center bg-transparent mb-3 p-4"><i class="fa fa-{{siteicon}}"></i> {{sitename}}</h2>
                        {{/logourl}}
                    {{/logopos}}
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'logopos' section
                $value = $context->find('logopos');
                $buffer .= $this->sectionC736c129c3a8458939f52c076edf4156($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section12e1b5bec73262d95b9705de2350bb10(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
								<div class="teacher">	
									<p>Become<br>
									a teacher process</p>
								</div>
								<div class="teacher_icon">
									<div class="floatLeft">
									<img src="https://allinschool.com/my/images/signup-icon.png"/>
									</div>
							 
									<h3 class="teacher_content"><b>Sign Up</b></h3>
									<p class="req">We request the teachers to create an account on <br>Allinschool by filling this Sign Up form.</p>
								</div><br>
								<div class="teacher_icon">
									<img src="https://allinschool.com/my/images/create-teacher-profile.png"/ class="floatLeft">
									
									<h3 class="teacher_content"><b>Create teacher profile</b></h3>
									<p class="req">After signing up, please fill in your profile details & submit it for screening & approval.</p>
								</div><br>
								<div class="teacher_icon">
									<img src="https://allinschool.com/my/images/teach-courses-icon.png"/ class="floatLeft">
									
									<h3 class="teacher_content"><b>Screening & approval</b></h3>
									<p class="req">Allinschool will conduct a small screening & approval process in order to maintain the teaching quality on Allinschool. For this, Allinschool will take up to 24 hours.</p>
								</div><br>
								<div class="teacher_icon">
									<img src="https://allinschool.com/my/images/teach-courses-icon.png"/ class="floatLeft">
									
									<h3 class="teacher_content"><b>Create & teach courses</b></h3>
									<p class="req">After the teacher profile & bio is approved, the teacher will be able to create, publish and teach courses on Allinschool.</p>
								</div><br>
							';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '								<div class="teacher">	
';
                $buffer .= $indent . '									<p>Become<br>
';
                $buffer .= $indent . '									a teacher process</p>
';
                $buffer .= $indent . '								</div>
';
                $buffer .= $indent . '								<div class="teacher_icon">
';
                $buffer .= $indent . '									<div class="floatLeft">
';
                $buffer .= $indent . '									<img src="https://allinschool.com/my/images/signup-icon.png"/>
';
                $buffer .= $indent . '									</div>
';
                $buffer .= $indent . '							 
';
                $buffer .= $indent . '									<h3 class="teacher_content"><b>Sign Up</b></h3>
';
                $buffer .= $indent . '									<p class="req">We request the teachers to create an account on <br>Allinschool by filling this Sign Up form.</p>
';
                $buffer .= $indent . '								</div><br>
';
                $buffer .= $indent . '								<div class="teacher_icon">
';
                $buffer .= $indent . '									<img src="https://allinschool.com/my/images/create-teacher-profile.png"/ class="floatLeft">
';
                $buffer .= $indent . '									
';
                $buffer .= $indent . '									<h3 class="teacher_content"><b>Create teacher profile</b></h3>
';
                $buffer .= $indent . '									<p class="req">After signing up, please fill in your profile details & submit it for screening & approval.</p>
';
                $buffer .= $indent . '								</div><br>
';
                $buffer .= $indent . '								<div class="teacher_icon">
';
                $buffer .= $indent . '									<img src="https://allinschool.com/my/images/teach-courses-icon.png"/ class="floatLeft">
';
                $buffer .= $indent . '									
';
                $buffer .= $indent . '									<h3 class="teacher_content"><b>Screening & approval</b></h3>
';
                $buffer .= $indent . '									<p class="req">Allinschool will conduct a small screening & approval process in order to maintain the teaching quality on Allinschool. For this, Allinschool will take up to 24 hours.</p>
';
                $buffer .= $indent . '								</div><br>
';
                $buffer .= $indent . '								<div class="teacher_icon">
';
                $buffer .= $indent . '									<img src="https://allinschool.com/my/images/teach-courses-icon.png"/ class="floatLeft">
';
                $buffer .= $indent . '									
';
                $buffer .= $indent . '									<h3 class="teacher_content"><b>Create & teach courses</b></h3>
';
                $buffer .= $indent . '									<p class="req">After the teacher profile & bio is approved, the teacher will be able to create, publish and teach courses on Allinschool.</p>
';
                $buffer .= $indent . '								</div><br>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionAc190bacb4c6349c118f0fe9033b93e9(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'signup, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'signup, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7d62c0f250d2caa9b153da8ab6cc6e0f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
require([\'theme_remui/loader\'], function () {
    jQuery(document).ready(function(){
        jQuery(\'#page-login-signup label[for="id_recaptcha_element"]\').parent().css({"width": "100%", "margin": "0 20px"});
    });
});
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . 'require([\'theme_remui/loader\'], function () {
';
                $buffer .= $indent . '    jQuery(document).ready(function(){
';
                $buffer .= $indent . '        jQuery(\'#page-login-signup label[for="id_recaptcha_element"]\').parent().css({"width": "100%", "margin": "0 20px"});
';
                $buffer .= $indent . '    });
';
                $buffer .= $indent . '});
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
