<?php

class __Mustache_fbd6f683df564492d6eee50eba590bdf extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<div class="row category-filters pb-3 d-none" aria-label="Course Archive Page Filters">
';
        $buffer .= $indent . '    <div class="my-1 col-lg-3 col-md-6 col-sm-12">
';
        $buffer .= $indent . '        <select id="categoryfilter" class="selectpicker" aria-haspopup="true" aria-expanded="false" data-live-search="true" data-style="custom-select form-control bg-white">
';
        $buffer .= $indent . '            <option value=\'all\'>';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionD88f2d38c71db2f707c6d70812a2529b($context, $indent, $value);
        $buffer .= '</option>
';
        // 'categories' section
        $value = $context->find('categories');
        $buffer .= $this->section42d5edb1ebe645394909bb1a4b90cb0e($context, $indent, $value);
        $buffer .= $indent . '        </select>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '    <div class="my-1 col-lg-3 col-md-6 col-sm-12">
';
        $buffer .= $indent . '        <select id="sortfilter" class="selectpicker" data-style="custom-select form-control bg-white">
';
        $buffer .= $indent . '            <option value=\'default\'>';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionA834a1a1327e810c33afcec2624a0ebb($context, $indent, $value);
        $buffer .= '</option>
';
        $buffer .= $indent . '            <option value=\'ASC\'>';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section671061cb6a745bd82990110eb6539ddd($context, $indent, $value);
        $buffer .= '</option>
';
        $buffer .= $indent . '            <option value=\'DESC\'>';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section0c71eea8c9ac0747819fd1b6cb6fbe29($context, $indent, $value);
        $buffer .= '</option>   
';
        $buffer .= $indent . '        </select>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '    <div class="my-1 col-lg-4 col-md-9 col-sm-12">
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->find('searchhtml'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '    </div>
';
        // 'latest_card' inverted section
        $value = $context->find('latest_card');
        if (empty($value)) {
            
            $buffer .= $indent . '    <div class="my-1 col-lg-2 col-md-3 hidden-sm-down">
';
            $buffer .= $indent . '        <div class="d-flex float-right btn-group" data-toggle="buttons">
';
            $buffer .= $indent . '            <a class="grid_btn btn togglebtn active btn-primary" title="Grid view" data-view="grid" tabindex="0" role="button">
';
            $buffer .= $indent . '                <i class="fa fa-th" aria-hidden="true"></i>
';
            $buffer .= $indent . '            </a>
';
            $buffer .= $indent . '            <a class="list_btn btn togglebtn" title="List view" data-view="list" tabindex="0" role="button">
';
            $buffer .= $indent . '                <i class="fa fa-list" aria-hidden="true"></i>
';
            $buffer .= $indent . '            </a>
';
            $buffer .= $indent . '        </div>
';
            $buffer .= $indent . '    </div>
';
        }
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function sectionD88f2d38c71db2f707c6d70812a2529b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' allcategories , theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' allcategories , theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section42d5edb1ebe645394909bb1a4b90cb0e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <option value=\'{{ id }}\'>{{{ title }}}</option>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <option value=\'';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '\'>';
                $value = $this->resolveValue($context->find('title'), $context);
                $buffer .= $value;
                $buffer .= '</option>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA834a1a1327e810c33afcec2624a0ebb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' sortdefault, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' sortdefault, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section671061cb6a745bd82990110eb6539ddd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' sortascending, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' sortascending, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section0c71eea8c9ac0747819fd1b6cb6fbe29(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' sortdescending, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' sortdescending, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
