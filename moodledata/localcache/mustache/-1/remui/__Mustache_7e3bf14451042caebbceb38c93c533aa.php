<?php

class __Mustache_7e3bf14451042caebbceb38c93c533aa extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '<li id="section-0" class="first-section-li section main clearfix mb-25 mb-4" role="region" aria-label="';
        $value = $this->resolveValue($context->find('generalsectiontitlename'), $context);
        $buffer .= $value;
        $buffer .= '">
';
        $buffer .= $indent . '   <div class="px-15 py-10 px-3 p-x-1 py-2 p-y-1 course-cover-image" style="background-image: linear-gradient(to right, rgba(14, 35, 53, 0.68), rgba(14, 35, 53, 0.68)), url(';
        $value = $this->resolveValue($context->find('remuicourseimage'), $context);
        $buffer .= $value;
        $buffer .= ');">
';
        // 'rightside' section
        $value = $context->find('rightside');
        $buffer .= $this->section066d2d98a2ca10a09b8efba7fa14dee8($context, $indent, $value);
        $buffer .= $indent . '        <div class="text-white">
';
        $buffer .= $indent . '            <h2 class="section-title">
';
        $buffer .= $indent . '                ';
        $value = $this->resolveValue($context->find('generalsectiontitle'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '            </h2>
';
        $buffer .= $indent . '            <div class="font-size-14 summary">';
        $value = $this->resolveValue($context->find('generalsectionsummary'), $context);
        $buffer .= $value;
        $buffer .= '</div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '        <div class="text-white mb-10 m-b-1 d-flex" style="justify-content: flex-end;">
';
        // 'teachers' section
        $value = $context->find('teachers');
        $buffer .= $this->section7a3af305fa869ad84ad83c527699ea3f($context, $indent, $value);
        $buffer .= $indent . '        </div>
';
        // 'percentage' section
        $value = $context->find('percentage');
        $buffer .= $this->section6ae781b2734f21213cfa9473ff20e76e($context, $indent, $value);
        $buffer .= $indent . '   </div>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '   <span class="hidden sectionname">';
        $value = $this->resolveValue($context->find('generalsectiontitlename'), $context);
        $buffer .= $value;
        $buffer .= '</span>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '   <div class="content" aria-label="';
        $value = $this->resolveValue($context->find('generalsectiontitlename'), $context);
        $buffer .= $value;
        $buffer .= '">
';
        $buffer .= $indent . '        <div class="section_availability badge badge-pill badge-info mb-10"></div>
';
        $buffer .= $indent . '            <div class="summary hide"></div>
';
        $buffer .= $indent . '            <ul class="section general-section-activities img-text row justify-content-start text-center">
';
        // 'activities' section
        $value = $context->find('activities');
        $buffer .= $this->section4173fb37a3c9297fecc96999f8010b3c($context, $indent, $value);
        $buffer .= $indent . '            </ul>
';
        $buffer .= $indent . '        <div class="text-center showactivitywrapper">
';
        $buffer .= $indent . '            <span class="showactivity text-primary show">
';
        $buffer .= $indent . '                <i class="fa fa-angle-down" aria-hidden="true"></i>
';
        $buffer .= $indent . '            </span>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->find('addnewactivity'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</li>
';

        return $buffer;
    }

    private function section066d2d98a2ca10a09b8efba7fa14dee8(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <div class="right side">
        {{{rightside}}}
    </div>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <div class="right side">
';
                $buffer .= $indent . '        ';
                $value = $this->resolveValue($context->find('rightside'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7a3af305fa869ad84ad83c527699ea3f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                {{{teacherimg}}}
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                ';
                $value = $this->resolveValue($context->find('teacherimg'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6ae781b2734f21213cfa9473ff20e76e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="progress progress-square mb-0">
                <div class="progress-bar bg-green-600" style="width: {{{percentage}}}%; height: 100%;" role="progressbar">
                    <span class="sr-only">{{{percentage}}}% Complete</span>
                </div>
            </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="progress progress-square mb-0">
';
                $buffer .= $indent . '                <div class="progress-bar bg-green-600" style="width: ';
                $value = $this->resolveValue($context->find('percentage'), $context);
                $buffer .= $value;
                $buffer .= '%; height: 100%;" role="progressbar">
';
                $buffer .= $indent . '                    <span class="sr-only">';
                $value = $this->resolveValue($context->find('percentage'), $context);
                $buffer .= $value;
                $buffer .= '% Complete</span>
';
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA3d5bb647fec37b52fa76ca4a1b93b42(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                    <div class="{{{indent}}}"></div>
                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                    <div class="';
                $value = $this->resolveValue($context->find('indent'), $context);
                $buffer .= $value;
                $buffer .= '"></div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4173fb37a3c9297fecc96999f8010b3c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    <li class="activity first-section-activity modtype_{{{modulename}}} yui3-dd-drop" id="module-{{id}}">
                        <div>
                            <span class="editing_move moodle-core-dragdrop-draghandle" title="Move resource" tabindex="0" data-draggroups="resource" role="button" data-sectionreturn="0"><i class="icon fa fa-arrows fa-fw  iconsmall" aria-hidden="true" aria-label="" style="cursor: move;"></i></span>
                            <div class="mod-indent-outer w-100">
                                {{#indent}}
                                    <div class="{{{indent}}}"></div>
                                {{/indent}}
                                <div>
                                    <div class="activityinstance p-0 d-inline-block">
                                        {{{title}}}
                                    </div>
                                    <div class="actions">
                                        {{{completion}}}
                                        {{{modicons}}}
                                    </div>
                                </div>
                                {{{availstatus}}}
                            </div>
                        </div>
                    </li>
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    <li class="activity first-section-activity modtype_';
                $value = $this->resolveValue($context->find('modulename'), $context);
                $buffer .= $value;
                $buffer .= ' yui3-dd-drop" id="module-';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">
';
                $buffer .= $indent . '                        <div>
';
                $buffer .= $indent . '                            <span class="editing_move moodle-core-dragdrop-draghandle" title="Move resource" tabindex="0" data-draggroups="resource" role="button" data-sectionreturn="0"><i class="icon fa fa-arrows fa-fw  iconsmall" aria-hidden="true" aria-label="" style="cursor: move;"></i></span>
';
                $buffer .= $indent . '                            <div class="mod-indent-outer w-100">
';
                // 'indent' section
                $value = $context->find('indent');
                $buffer .= $this->sectionA3d5bb647fec37b52fa76ca4a1b93b42($context, $indent, $value);
                $buffer .= $indent . '                                <div>
';
                $buffer .= $indent . '                                    <div class="activityinstance p-0 d-inline-block">
';
                $buffer .= $indent . '                                        ';
                $value = $this->resolveValue($context->find('title'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                    <div class="actions">
';
                $buffer .= $indent . '                                        ';
                $value = $this->resolveValue($context->find('completion'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '                                        ';
                $value = $this->resolveValue($context->find('modicons'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                </div>
';
                $buffer .= $indent . '                                ';
                $value = $this->resolveValue($context->find('availstatus'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '                            </div>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '                    </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
