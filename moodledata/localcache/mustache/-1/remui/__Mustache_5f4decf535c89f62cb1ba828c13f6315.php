<?php

class __Mustache_5f4decf535c89f62cb1ba828c13f6315 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        if ($partial = $this->mustache->loadPartial('theme_remui/common_start')) {
            $buffer .= $partial->renderInternal($context);
        }
        $buffer .= $indent . '    <div class="row m-0 p-0">
';
        // 'user' section
        $value = $context->find('user');
        $buffer .= $this->section08edeb9f259bd12b7c463986af116602($context, $indent, $value);
        // 'hasregionmainsettingsmenu' section
        $value = $context->find('hasregionmainsettingsmenu');
        $buffer .= $this->section9a68b904a18c619c434de2acc46a325c($context, $indent, $value);
        $buffer .= $indent . '                                            <section id="region-main"  class="p-0 ';
        // 'hasblocks' section
        $value = $context->find('hasblocks');
        $buffer .= $this->section8ae768dbd9f60a7f7df4aaf3cee7aa89($context, $indent, $value);
        $buffer .= '">
';
        $buffer .= $indent . '                                                <h2 class="d-none">Ignore-Placeholder</h2>
';
        // 'hasregionmainsettingsmenu' section
        $value = $context->find('hasregionmainsettingsmenu');
        $buffer .= $this->section12f69f16e080b82c520636ff3b4bfc4e($context, $indent, $value);
        $buffer .= $indent . '
';
        $buffer .= $indent . '                                                ';
        $value = $this->resolveValue($context->findDot('output.course_content_header'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                                                ';
        $value = $this->resolveValue($context->findDot('output.main_content'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                                                ';
        $value = $this->resolveValue($context->findDot('output.course_content_footer'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '                                            </section>
';
        // 'user' section
        $value = $context->find('user');
        $buffer .= $this->section7681ccb132cc2d1e1cb8aefb9a6c0c0f($context, $indent, $value);
        $buffer .= $indent . '    </div>
';
        if ($partial = $this->mustache->loadPartial('theme_remui/common_end')) {
            $buffer .= $partial->renderInternal($context);
        }
        $buffer .= $indent . '
';
        // 'js' section
        $value = $context->find('js');
        $buffer .= $this->section380b49d1abc26a2e8663155d70fddce3($context, $indent, $value);

        return $buffer;
    }

    private function sectionC9a9e23934810b835fedb59873e9478a(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' administrator, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' administrator, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7e284bd4b7a8346cb6e266d0999ebd52(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                {{^ notcurrentuser }}
                <p class="profile-job">{{# str }} administrator, theme_remui {{/ str }}</p>
                {{/ notcurrentuser }}
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'notcurrentuser' inverted section
                $value = $context->find('notcurrentuser');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                <p class="profile-job">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->sectionC9a9e23934810b835fedb59873e9478a($context, $indent, $value);
                    $buffer .= '</p>
';
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section439530f96bb6264a1c61b569cc219abd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' webpage, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' webpage, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionFf52b0eda4fc4aaaf000a843f60d74e8(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <a role="button" href="{{ url }}" target="_blank" class="btn btn-primary">{{# str }} webpage, theme_remui {{/ str }}</a>
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <a role="button" href="';
                $value = $this->resolveValue($context->find('url'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" target="_blank" class="btn btn-primary">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section439530f96bb6264a1c61b569cc219abd($context, $indent, $value);
                $buffer .= '</a>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6bbda7740b7494cdd7b6fb2c5517855f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' contacts, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' contacts, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2d0d8eb043dde03f58715b485a049ee7(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' discussions, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' discussions, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section442aae21ae8f97e78ea47581808df932(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' blogentries, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' blogentries, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9e1cbbdc1ce0110333c561df68185afd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    <div class="col">
                    <strong class="profile-stat-count">{{ blogpostcount }}</strong>
                    <span>{{# str }} blogentries, theme_remui {{/ str }}</span>
                    </div>
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    <div class="col">
';
                $buffer .= $indent . '                    <strong class="profile-stat-count">';
                $value = $this->resolveValue($context->find('blogpostcount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</strong>
';
                $buffer .= $indent . '                    <span>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section442aae21ae8f97e78ea47581808df932($context, $indent, $value);
                $buffer .= '</span>
';
                $buffer .= $indent . '                    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section279e3087add8a5b37e61559d404d6bdd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' {{ firstname }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('firstname'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section720da621e11b81c3d93c0d5a76321389(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' aboutme, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' aboutme, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEf77f9818184f6127f9fdad130070fa1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        <li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#aboutme" aria-controls="aboutme" role="tab">{{# notcurrentuser }} {{ firstname }}{{/ notcurrentuser}} {{^ notcurrentuser }}{{# str }} aboutme, theme_remui {{/ str }}{{/ notcurrentuser}}</a></li>
                        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                        <li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#aboutme" aria-controls="aboutme" role="tab">';
                // 'notcurrentuser' section
                $value = $context->find('notcurrentuser');
                $buffer .= $this->section279e3087add8a5b37e61559d404d6bdd($context, $indent, $value);
                $buffer .= ' ';
                // 'notcurrentuser' inverted section
                $value = $context->find('notcurrentuser');
                if (empty($value)) {
                    
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section720da621e11b81c3d93c0d5a76321389($context, $indent, $value);
                }
                $buffer .= '</a></li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9c2d3b7209d10f7d9c9f3571b6ad33a0(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' courses, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' courses, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section457eedaeccff3bbe0e3188fe4a4da19e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' editprofile, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' editprofile, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section12d6096ab1af614f356d6f9308df6f4f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        {{^ notcurrentuser }}
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#editprofile" aria-controls="editprofile" role="tab">{{# str }} editprofile, theme_remui {{/ str }}</a></li>
                        {{/ notcurrentuser }}
                        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'notcurrentuser' inverted section
                $value = $context->find('notcurrentuser');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#editprofile" aria-controls="editprofile" role="tab">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section457eedaeccff3bbe0e3188fe4a4da19e($context, $indent, $value);
                    $buffer .= '</a></li>
';
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section547795cd54d5de9995597c1ebdb8078b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' interests, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' interests, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section3d6d15b2393476224c43796227f9bee5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                                    <a href="{{{config.wwwroot}}}/tag/index.php?id={{id}}" class="badge badge-info">{{ rawname }}
                                                    </a>
                                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                                    <a href="';
                $value = $this->resolveValue($context->findDot('config.wwwroot'), $context);
                $buffer .= $value;
                $buffer .= '/tag/index.php?id=';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" class="badge badge-info">';
                $value = $this->resolveValue($context->find('rawname'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '
';
                $buffer .= $indent . '                                                    </a>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionB45fb12274753f8d27e71a95ef294b87(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} interests, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{# interests }}
                                                    <a href="{{{config.wwwroot}}}/tag/index.php?id={{id}}" class="badge badge-info">{{ rawname }}
                                                    </a>
                                                {{/ interests }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                <li class="list-group-item">
';
                $buffer .= $indent . '                                    <div class="media">
';
                $buffer .= $indent . '                                        <div class="media-body d-block">
';
                $buffer .= $indent . '                                            <h5>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section547795cd54d5de9995597c1ebdb8078b($context, $indent, $value);
                $buffer .= '</h5>
';
                $buffer .= $indent . '                                            <div>
';
                // 'interests' section
                $value = $context->find('interests');
                $buffer .= $this->section3d6d15b2393476224c43796227f9bee5($context, $indent, $value);
                $buffer .= $indent . '                                            </div>
';
                $buffer .= $indent . '                                        </div>
';
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionFf50e65695429bd134d0851c980b5dca(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' institution, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' institution, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9a1dc5d0d7df80d993e480262b64c630(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} institution, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{ department }} {{ institution }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                <li class="list-group-item">
';
                $buffer .= $indent . '                                    <div class="media">
';
                $buffer .= $indent . '                                        <div class="media-body d-block">
';
                $buffer .= $indent . '                                            <h5>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionFf50e65695429bd134d0851c980b5dca($context, $indent, $value);
                $buffer .= '</h5>
';
                $buffer .= $indent . '                                            <div>
';
                $buffer .= $indent . '                                                ';
                $value = $this->resolveValue($context->find('department'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('institution'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '
';
                $buffer .= $indent . '                                            </div>
';
                $buffer .= $indent . '                                        </div>
';
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section263d9951165bd94bea140a5de15afbc3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' location, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' location, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section09e8d6e6bc563fb5467b815bbf2118ce(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} location, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{ address }} {{city}} {{ country }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                <li class="list-group-item">
';
                $buffer .= $indent . '                                    <div class="media">
';
                $buffer .= $indent . '                                        <div class="media-body d-block">
';
                $buffer .= $indent . '                                            <h5>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section263d9951165bd94bea140a5de15afbc3($context, $indent, $value);
                $buffer .= '</h5>
';
                $buffer .= $indent . '                                            <div>
';
                $buffer .= $indent . '                                                ';
                $value = $this->resolveValue($context->find('address'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('city'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('country'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '
';
                $buffer .= $indent . '                                            </div>
';
                $buffer .= $indent . '                                        </div>
';
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section22c58c8a5fb74ba676e08137f07fc84c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' badges, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' badges, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7ab23c4b2e3300292a5e49f7f539d8c9(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                                        <li>
                                                            <img src="{{ imageurl }}" alt="{{ name }}" />
                                                            <a href="{{ link }}" class="text-center d-block font-weight-400 mt-10 blue-grey-500 font-size-14 font-weight-100" data-toggle="tooltip" title="{{ desc }}" data-placement="bottom">{{ name }}</a>
                                                        </li>
                                                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                                        <li>
';
                $buffer .= $indent . '                                                            <img src="';
                $value = $this->resolveValue($context->find('imageurl'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" alt="';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" />
';
                $buffer .= $indent . '                                                            <a href="';
                $value = $this->resolveValue($context->find('link'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" class="text-center d-block font-weight-400 mt-10 blue-grey-500 font-size-14 font-weight-100" data-toggle="tooltip" title="';
                $value = $this->resolveValue($context->find('desc'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" data-placement="bottom">';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</a>
';
                $buffer .= $indent . '                                                        </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section3e31c5e7a08937cbec38a6cb957ff465(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} badges, theme_remui {{/ str }}</h5>
                                            <div>
                                                <ul class="d-flex p-0 pt-3" style="list-style: none;">
                                                    {{# badges}}
                                                        <li>
                                                            <img src="{{ imageurl }}" alt="{{ name }}" />
                                                            <a href="{{ link }}" class="text-center d-block font-weight-400 mt-10 blue-grey-500 font-size-14 font-weight-100" data-toggle="tooltip" title="{{ desc }}" data-placement="bottom">{{ name }}</a>
                                                        </li>
                                                    {{/ badges}}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                <li class="list-group-item">
';
                $buffer .= $indent . '                                    <div class="media">
';
                $buffer .= $indent . '                                        <div class="media-body d-block">
';
                $buffer .= $indent . '                                            <h5>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section22c58c8a5fb74ba676e08137f07fc84c($context, $indent, $value);
                $buffer .= '</h5>
';
                $buffer .= $indent . '                                            <div>
';
                $buffer .= $indent . '                                                <ul class="d-flex p-0 pt-3" style="list-style: none;">
';
                // 'badges' section
                $value = $context->find('badges');
                $buffer .= $this->section7ab23c4b2e3300292a5e49f7f539d8c9($context, $indent, $value);
                $buffer .= $indent . '                                                </ul>
';
                $buffer .= $indent . '                                            </div>
';
                $buffer .= $indent . '                                        </div>
';
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionCfe809c1261369de1d6a19d2fac55ba0(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        <div class="tab-pane active animation-slide-left" id="aboutme" role="tabpanel">
                            <ul class="list-group">
                                {{# hasinterests }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} interests, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{# interests }}
                                                    <a href="{{{config.wwwroot}}}/tag/index.php?id={{id}}" class="badge badge-info">{{ rawname }}
                                                    </a>
                                                {{/ interests }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ hasinterests }}

                                {{# instidept }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} institution, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{ department }} {{ institution }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ instidept}}

                                {{# location }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} location, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{ address }} {{city}} {{ country }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ location}}

                                {{# hasbadges }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} badges, theme_remui {{/ str }}</h5>
                                            <div>
                                                <ul class="d-flex p-0 pt-3" style="list-style: none;">
                                                    {{# badges}}
                                                        <li>
                                                            <img src="{{ imageurl }}" alt="{{ name }}" />
                                                            <a href="{{ link }}" class="text-center d-block font-weight-400 mt-10 blue-grey-500 font-size-14 font-weight-100" data-toggle="tooltip" title="{{ desc }}" data-placement="bottom">{{ name }}</a>
                                                        </li>
                                                    {{/ badges}}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ hasbadges}}
                            </ul>
                        </div>
                        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                        <div class="tab-pane active animation-slide-left" id="aboutme" role="tabpanel">
';
                $buffer .= $indent . '                            <ul class="list-group">
';
                // 'hasinterests' section
                $value = $context->find('hasinterests');
                $buffer .= $this->sectionB45fb12274753f8d27e71a95ef294b87($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'instidept' section
                $value = $context->find('instidept');
                $buffer .= $this->section9a1dc5d0d7df80d993e480262b64c630($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'location' section
                $value = $context->find('location');
                $buffer .= $this->section09e8d6e6bc563fb5467b815bbf2118ce($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'hasbadges' section
                $value = $context->find('hasbadges');
                $buffer .= $this->section3e31c5e7a08937cbec38a6cb957ff465($context, $indent, $value);
                $buffer .= $indent . '                            </ul>
';
                $buffer .= $indent . '                        </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC4d90285e066c6afbd98148491b597ad(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' notenrolledanycourse, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' notenrolledanycourse, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionB24e118977d2b28c07f3718dea9c49ba(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' start_date, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' start_date, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionFbecad175d7ebb4e184f3f43ce6178ec(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' {{startdate}}, %A, %d %B %Y ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('startdate'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ', %A, %d %B %Y ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA259996413ee22bf5d83007e57c1fc46(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '<small>{{# str }} start_date, theme_remui {{/ str }} {{#userdate}} {{startdate}}, %A, %d %B %Y {{/userdate}}</small>';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '<small>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionB24e118977d2b28c07f3718dea9c49ba($context, $indent, $value);
                $buffer .= ' ';
                // 'userdate' section
                $value = $context->find('userdate');
                $buffer .= $this->sectionFbecad175d7ebb4e184f3f43ce6178ec($context, $indent, $value);
                $buffer .= '</small>';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA8deb82c646a9b7a94fd4077b9b9b36c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '{{ progress }}';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $value = $this->resolveValue($context->find('progress'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEbf40d8d478b7aafe9fb165783ddd8f8(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' complete, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' complete, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionAaebb440c8a290bd72b33569f000038b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                            <div class="float-right remui-course-progress w-full pie-progress pie-progress-sm"
                                                data-plugin="pieProgress"
                                                data-barcolor="#11c26d"
                                                data-size="100"
                                                data-barsize="6"
                                                data-goal="100"
                                                aria-valuenow="{{ progress }}"
                                                role="progressbar">
                                                <div class="pie-progress-content">
                                                <div class="pie-progress-number">{{#progress}}{{ progress }}{{/progress}}{{^progress}}0{{/progress}}&#37;</div>
                                                <div class="pie-progress-label">{{# str }} complete, theme_remui {{/ str }}</div>
                                                </div>
                                            </div>
                                            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                            <div class="float-right remui-course-progress w-full pie-progress pie-progress-sm"
';
                $buffer .= $indent . '                                                data-plugin="pieProgress"
';
                $buffer .= $indent . '                                                data-barcolor="#11c26d"
';
                $buffer .= $indent . '                                                data-size="100"
';
                $buffer .= $indent . '                                                data-barsize="6"
';
                $buffer .= $indent . '                                                data-goal="100"
';
                $buffer .= $indent . '                                                aria-valuenow="';
                $value = $this->resolveValue($context->find('progress'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"
';
                $buffer .= $indent . '                                                role="progressbar">
';
                $buffer .= $indent . '                                                <div class="pie-progress-content">
';
                $buffer .= $indent . '                                                <div class="pie-progress-number">';
                // 'progress' section
                $value = $context->find('progress');
                $buffer .= $this->sectionA8deb82c646a9b7a94fd4077b9b9b36c($context, $indent, $value);
                // 'progress' inverted section
                $value = $context->find('progress');
                if (empty($value)) {
                    
                    $buffer .= '0';
                }
                $buffer .= '&#37;</div>
';
                $buffer .= $indent . '                                                <div class="pie-progress-label">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionEbf40d8d478b7aafe9fb165783ddd8f8($context, $indent, $value);
                $buffer .= '</div>
';
                $buffer .= $indent . '                                                </div>
';
                $buffer .= $indent . '                                            </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7c5769f9e32abdbab2cceb0bf1edad61(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '

                                <li id="course-{{ id }}" class="list-group-item">
                                    <div class="media">
                                        <div class="pr-20 hidden-sm-down">
                                            <a class="avatar" href="{{ link }}">
                                                <img class="img-fluid" src="{{{ courseimage }}}" alt="...">
                                            </a>
                                        </div>

                                        <div class="media-body pr-10 pb-10">
                                            <a href="{{ link }}"><h5 class="mt-0">{{{ fullname }}}</h5></a>
                                            {{# startdate }}<small>{{# str }} start_date, theme_remui {{/ str }} {{#userdate}} {{startdate}}, %A, %d %B %Y {{/userdate}}</small>{{/ startdate }}

                                            <div class="profile-brief mt-10 hidden-sm-down">
                                                {{{ summary }}}
                                            </div>
                                        </div>
                                        {{# enablecompletion }}
                                            <div class="float-right remui-course-progress w-full pie-progress pie-progress-sm"
                                                data-plugin="pieProgress"
                                                data-barcolor="#11c26d"
                                                data-size="100"
                                                data-barsize="6"
                                                data-goal="100"
                                                aria-valuenow="{{ progress }}"
                                                role="progressbar">
                                                <div class="pie-progress-content">
                                                <div class="pie-progress-number">{{#progress}}{{ progress }}{{/progress}}{{^progress}}0{{/progress}}&#37;</div>
                                                <div class="pie-progress-label">{{# str }} complete, theme_remui {{/ str }}</div>
                                                </div>
                                            </div>
                                            {{/ enablecompletion }}
                                    </div>
                                </li>
                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '
';
                $buffer .= $indent . '                                <li id="course-';
                $value = $this->resolveValue($context->find('id'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" class="list-group-item">
';
                $buffer .= $indent . '                                    <div class="media">
';
                $buffer .= $indent . '                                        <div class="pr-20 hidden-sm-down">
';
                $buffer .= $indent . '                                            <a class="avatar" href="';
                $value = $this->resolveValue($context->find('link'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">
';
                $buffer .= $indent . '                                                <img class="img-fluid" src="';
                $value = $this->resolveValue($context->find('courseimage'), $context);
                $buffer .= $value;
                $buffer .= '" alt="...">
';
                $buffer .= $indent . '                                            </a>
';
                $buffer .= $indent . '                                        </div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                                        <div class="media-body pr-10 pb-10">
';
                $buffer .= $indent . '                                            <a href="';
                $value = $this->resolveValue($context->find('link'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"><h5 class="mt-0">';
                $value = $this->resolveValue($context->find('fullname'), $context);
                $buffer .= $value;
                $buffer .= '</h5></a>
';
                $buffer .= $indent . '                                            ';
                // 'startdate' section
                $value = $context->find('startdate');
                $buffer .= $this->sectionA259996413ee22bf5d83007e57c1fc46($context, $indent, $value);
                $buffer .= '
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                                            <div class="profile-brief mt-10 hidden-sm-down">
';
                $buffer .= $indent . '                                                ';
                $value = $this->resolveValue($context->find('summary'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '                                            </div>
';
                $buffer .= $indent . '                                        </div>
';
                // 'enablecompletion' section
                $value = $context->find('enablecompletion');
                $buffer .= $this->sectionAaebb440c8a290bd72b33569f000038b($context, $indent, $value);
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionEf02422b660be17619bb01deb6c5a1ad(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                {{# courses }}

                                <li id="course-{{ id }}" class="list-group-item">
                                    <div class="media">
                                        <div class="pr-20 hidden-sm-down">
                                            <a class="avatar" href="{{ link }}">
                                                <img class="img-fluid" src="{{{ courseimage }}}" alt="...">
                                            </a>
                                        </div>

                                        <div class="media-body pr-10 pb-10">
                                            <a href="{{ link }}"><h5 class="mt-0">{{{ fullname }}}</h5></a>
                                            {{# startdate }}<small>{{# str }} start_date, theme_remui {{/ str }} {{#userdate}} {{startdate}}, %A, %d %B %Y {{/userdate}}</small>{{/ startdate }}

                                            <div class="profile-brief mt-10 hidden-sm-down">
                                                {{{ summary }}}
                                            </div>
                                        </div>
                                        {{# enablecompletion }}
                                            <div class="float-right remui-course-progress w-full pie-progress pie-progress-sm"
                                                data-plugin="pieProgress"
                                                data-barcolor="#11c26d"
                                                data-size="100"
                                                data-barsize="6"
                                                data-goal="100"
                                                aria-valuenow="{{ progress }}"
                                                role="progressbar">
                                                <div class="pie-progress-content">
                                                <div class="pie-progress-number">{{#progress}}{{ progress }}{{/progress}}{{^progress}}0{{/progress}}&#37;</div>
                                                <div class="pie-progress-label">{{# str }} complete, theme_remui {{/ str }}</div>
                                                </div>
                                            </div>
                                            {{/ enablecompletion }}
                                    </div>
                                </li>
                                {{/ courses }}
                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'courses' section
                $value = $context->find('courses');
                $buffer .= $this->section7c5769f9e32abdbab2cceb0bf1edad61($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section08edeb9f259bd12b7c463986af116602(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
        <div class="col-lg-3">
            <!-- Page Widget -->
            <div class="card card-shadow text-center border-0">
                <div class="card-block">
                <a class="avatar avatar-lg" href="javascript:void(0)">
                    <img class="w-100" src="{{ profilepicture }}" alt="{{ firstname }} {{ lastname }}">
                </a>
                <h4 class="profile-user">{{ firstname }} {{ lastname }}</h4>
                {{# usercanmanage }}
                {{^ notcurrentuser }}
                <p class="profile-job">{{# str }} administrator, theme_remui {{/ str }}</p>
                {{/ notcurrentuser }}
                {{/ usercanmanage }}

                <div id="user-description"><p>{{{ description }}}</p></div>

                <div class="profile-social">
                    <a class="icon fa fa-skype" target="_blank" href="{{ skype }}"></a>
                    <a class="icon fa fa-yahoo" target="_blank" href="{{ yahoo }}"></a>
                    <a class="icon fa fa-windows" target="_blank" href="{{ msn }}"></a>
                </div>
                {{# url }}
                <a role="button" href="{{ url }}" target="_blank" class="btn btn-primary">{{# str }} webpage, theme_remui {{/ str }}</a>
                {{/ url }}
                </div>
                <div class="card-footer">
                <div class="row no-space">
                    <div class="col">
                    <strong class="profile-stat-count">{{ contactscount }}</strong>
                    <span>{{# str }} contacts, theme_remui {{/ str }}</span>
                    </div>
                    <div class="col">
                    <strong class="profile-stat-count">{{ forumpostcount }}</strong>
                    <span>{{# str }} discussions, theme_remui {{/ str }}</span>
                    </div>
                    {{# blogpostcount }}
                    <div class="col">
                    <strong class="profile-stat-count">{{ blogpostcount }}</strong>
                    <span>{{# str }} blogentries, theme_remui {{/ str }}</span>
                    </div>
                    {{/ blogpostcount }}
                </div>
                </div>
            </div>
            <!-- End Page Widget -->
        </div>

        <div class="col-lg-9">
            <!-- Panel -->
            <div class="panel">
                <div class="panel-body nav-tabs-animate nav-tabs-horizontal overflow-hidden profile-tabs" data-plugin="tabs">
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                        {{# aboutme }}
                        <li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#aboutme" aria-controls="aboutme" role="tab">{{# notcurrentuser }} {{ firstname }}{{/ notcurrentuser}} {{^ notcurrentuser }}{{# str }} aboutme, theme_remui {{/ str }}{{/ notcurrentuser}}</a></li>
                        {{/ aboutme }}
                        <li class="nav-item" role="presentation"><a class="nav-link {{^ aboutme }}active{{/ aboutme }}" data-toggle="tab" href="#usercourses" aria-controls="usercourses" role="tab">{{# str }} courses, theme_remui {{/ str }}</a></li>
                        {{#haseditpermission}}
                        {{^ notcurrentuser }}
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#editprofile" aria-controls="editprofile" role="tab">{{# str }} editprofile, theme_remui {{/ str }}</a></li>
                        {{/ notcurrentuser }}
                        {{/haseditpermission}}
                        <li class="nav-item ml-auto" role="presentation"><a class="nav-link" data-toggle="tab" href="#userprefs" aria-controls="userprefs" role="tab"><i class="icon fa fa-cog"></i></a></li>
                    </ul>

                    <div class="tab-content">
                        {{# aboutme }}
                        <div class="tab-pane active animation-slide-left" id="aboutme" role="tabpanel">
                            <ul class="list-group">
                                {{# hasinterests }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} interests, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{# interests }}
                                                    <a href="{{{config.wwwroot}}}/tag/index.php?id={{id}}" class="badge badge-info">{{ rawname }}
                                                    </a>
                                                {{/ interests }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ hasinterests }}

                                {{# instidept }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} institution, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{ department }} {{ institution }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ instidept}}

                                {{# location }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} location, theme_remui {{/ str }}</h5>
                                            <div>
                                                {{ address }} {{city}} {{ country }}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ location}}

                                {{# hasbadges }}
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="media-body d-block">
                                            <h5>{{# str }} badges, theme_remui {{/ str }}</h5>
                                            <div>
                                                <ul class="d-flex p-0 pt-3" style="list-style: none;">
                                                    {{# badges}}
                                                        <li>
                                                            <img src="{{ imageurl }}" alt="{{ name }}" />
                                                            <a href="{{ link }}" class="text-center d-block font-weight-400 mt-10 blue-grey-500 font-size-14 font-weight-100" data-toggle="tooltip" title="{{ desc }}" data-placement="bottom">{{ name }}</a>
                                                        </li>
                                                    {{/ badges}}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {{/ hasbadges}}
                            </ul>
                        </div>
                        {{/ aboutme }}

                        <div class="tab-pane  {{^ aboutme }}active show{{/ aboutme }} animation-slide-left" id="usercourses" role="tabpanel">
                            <ul class="list-group">
                                {{^ hascourses }}
                                    <li class="list-group-item border-0">
                                        <div class="media">
                                            <div class="media-body">
                                                {{# str }} notenrolledanycourse, theme_remui {{/ str }}
                                            </div>
                                        </div>
                                    </li>
                                {{/ hascourses }}
                                {{# hascourses }}
                                {{# courses }}

                                <li id="course-{{ id }}" class="list-group-item">
                                    <div class="media">
                                        <div class="pr-20 hidden-sm-down">
                                            <a class="avatar" href="{{ link }}">
                                                <img class="img-fluid" src="{{{ courseimage }}}" alt="...">
                                            </a>
                                        </div>

                                        <div class="media-body pr-10 pb-10">
                                            <a href="{{ link }}"><h5 class="mt-0">{{{ fullname }}}</h5></a>
                                            {{# startdate }}<small>{{# str }} start_date, theme_remui {{/ str }} {{#userdate}} {{startdate}}, %A, %d %B %Y {{/userdate}}</small>{{/ startdate }}

                                            <div class="profile-brief mt-10 hidden-sm-down">
                                                {{{ summary }}}
                                            </div>
                                        </div>
                                        {{# enablecompletion }}
                                            <div class="float-right remui-course-progress w-full pie-progress pie-progress-sm"
                                                data-plugin="pieProgress"
                                                data-barcolor="#11c26d"
                                                data-size="100"
                                                data-barsize="6"
                                                data-goal="100"
                                                aria-valuenow="{{ progress }}"
                                                role="progressbar">
                                                <div class="pie-progress-content">
                                                <div class="pie-progress-number">{{#progress}}{{ progress }}{{/progress}}{{^progress}}0{{/progress}}&#37;</div>
                                                <div class="pie-progress-label">{{# str }} complete, theme_remui {{/ str }}</div>
                                                </div>
                                            </div>
                                            {{/ enablecompletion }}
                                    </div>
                                </li>
                                {{/ courses }}
                                {{/ hascourses }}
                            </ul>
                        </div>

                        <div class="tab-pane animation-slide-left float-right" id="userprefs" role="tabpanel">
                            <ul class="list-group">
                                <li class="list-group-item p-0">
                                    <div class="media">
                                        <div class="media-body">
  ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '        <div class="col-lg-3">
';
                $buffer .= $indent . '            <!-- Page Widget -->
';
                $buffer .= $indent . '            <div class="card card-shadow text-center border-0">
';
                $buffer .= $indent . '                <div class="card-block">
';
                $buffer .= $indent . '                <a class="avatar avatar-lg" href="javascript:void(0)">
';
                $buffer .= $indent . '                    <img class="w-100" src="';
                $value = $this->resolveValue($context->find('profilepicture'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" alt="';
                $value = $this->resolveValue($context->find('firstname'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('lastname'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">
';
                $buffer .= $indent . '                </a>
';
                $buffer .= $indent . '                <h4 class="profile-user">';
                $value = $this->resolveValue($context->find('firstname'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('lastname'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</h4>
';
                // 'usercanmanage' section
                $value = $context->find('usercanmanage');
                $buffer .= $this->section7e284bd4b7a8346cb6e266d0999ebd52($context, $indent, $value);
                $buffer .= $indent . '
';
                $buffer .= $indent . '                <div id="user-description"><p>';
                $value = $this->resolveValue($context->find('description'), $context);
                $buffer .= $value;
                $buffer .= '</p></div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                <div class="profile-social">
';
                $buffer .= $indent . '                    <a class="icon fa fa-skype" target="_blank" href="';
                $value = $this->resolveValue($context->find('skype'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"></a>
';
                $buffer .= $indent . '                    <a class="icon fa fa-yahoo" target="_blank" href="';
                $value = $this->resolveValue($context->find('yahoo'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"></a>
';
                $buffer .= $indent . '                    <a class="icon fa fa-windows" target="_blank" href="';
                $value = $this->resolveValue($context->find('msn'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '"></a>
';
                $buffer .= $indent . '                </div>
';
                // 'url' section
                $value = $context->find('url');
                $buffer .= $this->sectionFf52b0eda4fc4aaaf000a843f60d74e8($context, $indent, $value);
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '                <div class="card-footer">
';
                $buffer .= $indent . '                <div class="row no-space">
';
                $buffer .= $indent . '                    <div class="col">
';
                $buffer .= $indent . '                    <strong class="profile-stat-count">';
                $value = $this->resolveValue($context->find('contactscount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</strong>
';
                $buffer .= $indent . '                    <span>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section6bbda7740b7494cdd7b6fb2c5517855f($context, $indent, $value);
                $buffer .= '</span>
';
                $buffer .= $indent . '                    </div>
';
                $buffer .= $indent . '                    <div class="col">
';
                $buffer .= $indent . '                    <strong class="profile-stat-count">';
                $value = $this->resolveValue($context->find('forumpostcount'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</strong>
';
                $buffer .= $indent . '                    <span>';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section2d0d8eb043dde03f58715b485a049ee7($context, $indent, $value);
                $buffer .= '</span>
';
                $buffer .= $indent . '                    </div>
';
                // 'blogpostcount' section
                $value = $context->find('blogpostcount');
                $buffer .= $this->section9e1cbbdc1ce0110333c561df68185afd($context, $indent, $value);
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '            <!-- End Page Widget -->
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '        <div class="col-lg-9">
';
                $buffer .= $indent . '            <!-- Panel -->
';
                $buffer .= $indent . '            <div class="panel">
';
                $buffer .= $indent . '                <div class="panel-body nav-tabs-animate nav-tabs-horizontal overflow-hidden profile-tabs" data-plugin="tabs">
';
                $buffer .= $indent . '                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
';
                // 'aboutme' section
                $value = $context->find('aboutme');
                $buffer .= $this->sectionEf77f9818184f6127f9fdad130070fa1($context, $indent, $value);
                $buffer .= $indent . '                        <li class="nav-item" role="presentation"><a class="nav-link ';
                // 'aboutme' inverted section
                $value = $context->find('aboutme');
                if (empty($value)) {
                    
                    $buffer .= 'active';
                }
                $buffer .= '" data-toggle="tab" href="#usercourses" aria-controls="usercourses" role="tab">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section9c2d3b7209d10f7d9c9f3571b6ad33a0($context, $indent, $value);
                $buffer .= '</a></li>
';
                // 'haseditpermission' section
                $value = $context->find('haseditpermission');
                $buffer .= $this->section12d6096ab1af614f356d6f9308df6f4f($context, $indent, $value);
                $buffer .= $indent . '                        <li class="nav-item ml-auto" role="presentation"><a class="nav-link" data-toggle="tab" href="#userprefs" aria-controls="userprefs" role="tab"><i class="icon fa fa-cog"></i></a></li>
';
                $buffer .= $indent . '                    </ul>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                    <div class="tab-content">
';
                // 'aboutme' section
                $value = $context->find('aboutme');
                $buffer .= $this->sectionCfe809c1261369de1d6a19d2fac55ba0($context, $indent, $value);
                $buffer .= $indent . '
';
                $buffer .= $indent . '                        <div class="tab-pane  ';
                // 'aboutme' inverted section
                $value = $context->find('aboutme');
                if (empty($value)) {
                    
                    $buffer .= 'active show';
                }
                $buffer .= ' animation-slide-left" id="usercourses" role="tabpanel">
';
                $buffer .= $indent . '                            <ul class="list-group">
';
                // 'hascourses' inverted section
                $value = $context->find('hascourses');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                                    <li class="list-group-item border-0">
';
                    $buffer .= $indent . '                                        <div class="media">
';
                    $buffer .= $indent . '                                            <div class="media-body">
';
                    $buffer .= $indent . '                                                ';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->sectionC4d90285e066c6afbd98148491b597ad($context, $indent, $value);
                    $buffer .= '
';
                    $buffer .= $indent . '                                            </div>
';
                    $buffer .= $indent . '                                        </div>
';
                    $buffer .= $indent . '                                    </li>
';
                }
                // 'hascourses' section
                $value = $context->find('hascourses');
                $buffer .= $this->sectionEf02422b660be17619bb01deb6c5a1ad($context, $indent, $value);
                $buffer .= $indent . '                            </ul>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                        <div class="tab-pane animation-slide-left float-right" id="userprefs" role="tabpanel">
';
                $buffer .= $indent . '                            <ul class="list-group">
';
                $buffer .= $indent . '                                <li class="list-group-item p-0">
';
                $buffer .= $indent . '                                    <div class="media">
';
                $buffer .= $indent . '                                        <div class="media-body">
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8ae768dbd9f60a7f7df4aaf3cee7aa89(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'has-blocks';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'has-blocks';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9a68b904a18c619c434de2acc46a325c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                                <div id="region-main-settings-menu" class="hidden-print {{#hasblocks}}has-blocks{{/hasblocks}}">
                                                    <div> {{{ output.region_main_settings_menu }}} </div>
                                                </div>
                                            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                                <div id="region-main-settings-menu" class="hidden-print ';
                // 'hasblocks' section
                $value = $context->find('hasblocks');
                $buffer .= $this->section8ae768dbd9f60a7f7df4aaf3cee7aa89($context, $indent, $value);
                $buffer .= '">
';
                $buffer .= $indent . '                                                    <div> ';
                $value = $this->resolveValue($context->findDot('output.region_main_settings_menu'), $context);
                $buffer .= $value;
                $buffer .= ' </div>
';
                $buffer .= $indent . '                                                </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section12f69f16e080b82c520636ff3b4bfc4e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                                    <div class="region_main_settings_menu_proxy"></div>
                                                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                                    <div class="region_main_settings_menu_proxy"></div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8005bcfe70be10e0ad7075f725cad663(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' firstname, moodle ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' firstname, moodle ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC87198b06c7ce18b3aeed72afb34afb9(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'disabled';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'disabled';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section58e3a0ceda913a38fcd0cbf3fd5832c5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' surname, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' surname, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5a84fe53ce69a3f2b53fe55b474c1002(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' email, moodle ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' email, moodle ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4246c0864d124cb365236d25ac28057b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' city, moodle ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' city, moodle ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section3030210d51638ae5cf921212ef7927f3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '{{city}} ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $value = $this->resolveValue($context->find('city'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7c6105c3642fd87d854505522c7f7953(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' country, moodle ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' country, moodle ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionAc91c3d45fd935b3f71812cf58ca375e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                                    <option value="{{ keyName }}">{{ valName }}</option>
                                                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                                    <option value="';
                $value = $this->resolveValue($context->find('keyName'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->find('valName'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</option>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section328481c5da6afee70239ef4306a8eda2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' description, moodle ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' description, moodle ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4fdbbf4fe528f5de5490718fe9c255a5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' submit, moodle ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' submit, moodle ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section41463bad4ba39b6216f3907fb4193103(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                        {{^ notcurrentuser }}
                            <div class="tab-pane animation-slide-left" id="editprofile" role="tabpanel">
                                <div class="panel-body">
                                    <form class="form-horizontal fv-form fv-form-bootstrap4" id="exampleStandardForm" autocomplete="off" novalidate="novalidate">
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} firstname, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id=\'first_name\' name="first_name" data-fv-field="first_name" value="{{ firstname }}" {{#field_lock_firstname}}disabled{{/field_lock_firstname}} >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} surname, theme_remui {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id=\'surname\' name="surname" data-fv-field="surname" value="{{ lastname }}" {{#field_lock_lastname}}disabled{{/field_lock_lastname}} >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} email, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" id=\'standard_email\' name="standard_email" data-fv-field="standard_email" value="{{ email }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} city, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id=\'city\' name="city" data-fv-field="city" value="{{#location}}{{city}} {{/location}}" {{#field_lock_city}}disabled{{/field_lock_city}}>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="col-md-3 form-control-label">{{# str }} country, moodle {{/ str }}</label>
                                                <div class="col-md-9">
                                                <select class="form-control" id="country" name="country" required="" data-fv-field="country" {{#field_lock_country}}disabled{{/field_lock_country}}>
                                                    {{# countries }}
                                                    <option value="{{ keyName }}">{{ valName }}</option>
                                                    {{/ countries }}
                                                </select>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} description, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                            <textarea class="form-control" id=\'description\' name="description" rows="3" data-fv-field="description">{{{description}}}</textarea>
                                            </div>
                                        </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary" id="btn-save-changes">{{# str }} submit, moodle {{/ str }}</button>
                                    </div>
                                </form>
                                    <!--<div class="alert alert-danger" id="error-message" style="display:none;margin-top:10px"></div>-->
                                    <div class="summary-errors alert alert-success alert-dismissible" id="error-message" style="display:none;margin-top:10px">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        {{/ notcurrentuser }}
                        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'notcurrentuser' inverted section
                $value = $context->find('notcurrentuser');
                if (empty($value)) {
                    
                    $buffer .= $indent . '                            <div class="tab-pane animation-slide-left" id="editprofile" role="tabpanel">
';
                    $buffer .= $indent . '                                <div class="panel-body">
';
                    $buffer .= $indent . '                                    <form class="form-horizontal fv-form fv-form-bootstrap4" id="exampleStandardForm" autocomplete="off" novalidate="novalidate">
';
                    $buffer .= $indent . '                                        <div class="form-group row">
';
                    $buffer .= $indent . '                                            <label class="col-md-3 form-control-label">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section8005bcfe70be10e0ad7075f725cad663($context, $indent, $value);
                    $buffer .= '</label>
';
                    $buffer .= $indent . '                                            <div class="col-md-9">
';
                    $buffer .= $indent . '                                                <input type="text" class="form-control" id=\'first_name\' name="first_name" data-fv-field="first_name" value="';
                    $value = $this->resolveValue($context->find('firstname'), $context);
                    $buffer .= call_user_func($this->mustache->getEscape(), $value);
                    $buffer .= '" ';
                    // 'field_lock_firstname' section
                    $value = $context->find('field_lock_firstname');
                    $buffer .= $this->sectionC87198b06c7ce18b3aeed72afb34afb9($context, $indent, $value);
                    $buffer .= ' >
';
                    $buffer .= $indent . '                                            </div>
';
                    $buffer .= $indent . '                                        </div>
';
                    $buffer .= $indent . '                                        <div class="form-group row">
';
                    $buffer .= $indent . '                                            <label class="col-md-3 form-control-label">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section58e3a0ceda913a38fcd0cbf3fd5832c5($context, $indent, $value);
                    $buffer .= '</label>
';
                    $buffer .= $indent . '                                            <div class="col-md-9">
';
                    $buffer .= $indent . '                                                <input type="text" class="form-control" id=\'surname\' name="surname" data-fv-field="surname" value="';
                    $value = $this->resolveValue($context->find('lastname'), $context);
                    $buffer .= call_user_func($this->mustache->getEscape(), $value);
                    $buffer .= '" ';
                    // 'field_lock_lastname' section
                    $value = $context->find('field_lock_lastname');
                    $buffer .= $this->sectionC87198b06c7ce18b3aeed72afb34afb9($context, $indent, $value);
                    $buffer .= ' >
';
                    $buffer .= $indent . '                                            </div>
';
                    $buffer .= $indent . '                                        </div>
';
                    $buffer .= $indent . '                                        <div class="form-group row">
';
                    $buffer .= $indent . '                                            <label class="col-md-3 form-control-label">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section5a84fe53ce69a3f2b53fe55b474c1002($context, $indent, $value);
                    $buffer .= '</label>
';
                    $buffer .= $indent . '                                            <div class="col-md-9">
';
                    $buffer .= $indent . '                                                <input type="email" class="form-control" id=\'standard_email\' name="standard_email" data-fv-field="standard_email" value="';
                    $value = $this->resolveValue($context->find('email'), $context);
                    $buffer .= call_user_func($this->mustache->getEscape(), $value);
                    $buffer .= '" disabled>
';
                    $buffer .= $indent . '                                            </div>
';
                    $buffer .= $indent . '                                        </div>
';
                    $buffer .= $indent . '                                        <div class="form-group row">
';
                    $buffer .= $indent . '                                            <label class="col-md-3 form-control-label">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section4246c0864d124cb365236d25ac28057b($context, $indent, $value);
                    $buffer .= '</label>
';
                    $buffer .= $indent . '                                            <div class="col-md-9">
';
                    $buffer .= $indent . '                                                <input type="text" class="form-control" id=\'city\' name="city" data-fv-field="city" value="';
                    // 'location' section
                    $value = $context->find('location');
                    $buffer .= $this->section3030210d51638ae5cf921212ef7927f3($context, $indent, $value);
                    $buffer .= '" ';
                    // 'field_lock_city' section
                    $value = $context->find('field_lock_city');
                    $buffer .= $this->sectionC87198b06c7ce18b3aeed72afb34afb9($context, $indent, $value);
                    $buffer .= '>
';
                    $buffer .= $indent . '                                            </div>
';
                    $buffer .= $indent . '                                        </div>
';
                    $buffer .= $indent . '                                        <div class="form-group row">
';
                    $buffer .= $indent . '                                                <label class="col-md-3 form-control-label">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section7c6105c3642fd87d854505522c7f7953($context, $indent, $value);
                    $buffer .= '</label>
';
                    $buffer .= $indent . '                                                <div class="col-md-9">
';
                    $buffer .= $indent . '                                                <select class="form-control" id="country" name="country" required="" data-fv-field="country" ';
                    // 'field_lock_country' section
                    $value = $context->find('field_lock_country');
                    $buffer .= $this->sectionC87198b06c7ce18b3aeed72afb34afb9($context, $indent, $value);
                    $buffer .= '>
';
                    // 'countries' section
                    $value = $context->find('countries');
                    $buffer .= $this->sectionAc91c3d45fd935b3f71812cf58ca375e($context, $indent, $value);
                    $buffer .= $indent . '                                                </select>
';
                    $buffer .= $indent . '                                                </div>
';
                    $buffer .= $indent . '                                        </div>
';
                    $buffer .= $indent . '                                        <div class="form-group row">
';
                    $buffer .= $indent . '                                            <label class="col-md-3 form-control-label">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section328481c5da6afee70239ef4306a8eda2($context, $indent, $value);
                    $buffer .= '</label>
';
                    $buffer .= $indent . '                                            <div class="col-md-9">
';
                    $buffer .= $indent . '                                            <textarea class="form-control" id=\'description\' name="description" rows="3" data-fv-field="description">';
                    $value = $this->resolveValue($context->find('description'), $context);
                    $buffer .= $value;
                    $buffer .= '</textarea>
';
                    $buffer .= $indent . '                                            </div>
';
                    $buffer .= $indent . '                                        </div>
';
                    $buffer .= $indent . '                                    <div class="text-right">
';
                    $buffer .= $indent . '                                        <button type="button" class="btn btn-primary" id="btn-save-changes">';
                    // 'str' section
                    $value = $context->find('str');
                    $buffer .= $this->section4fdbbf4fe528f5de5490718fe9c255a5($context, $indent, $value);
                    $buffer .= '</button>
';
                    $buffer .= $indent . '                                    </div>
';
                    $buffer .= $indent . '                                </form>
';
                    $buffer .= $indent . '                                    <!--<div class="alert alert-danger" id="error-message" style="display:none;margin-top:10px"></div>-->
';
                    $buffer .= $indent . '                                    <div class="summary-errors alert alert-success alert-dismissible" id="error-message" style="display:none;margin-top:10px">
';
                    $buffer .= $indent . '                                        <p></p>
';
                    $buffer .= $indent . '                                    </div>
';
                    $buffer .= $indent . '                                </div>
';
                    $buffer .= $indent . '                            </div>
';
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7681ccb132cc2d1e1cb8aefb9a6c0c0f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <!-- edit profile tab content -->
                        {{#haseditpermission}}
                        {{^ notcurrentuser }}
                            <div class="tab-pane animation-slide-left" id="editprofile" role="tabpanel">
                                <div class="panel-body">
                                    <form class="form-horizontal fv-form fv-form-bootstrap4" id="exampleStandardForm" autocomplete="off" novalidate="novalidate">
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} firstname, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id=\'first_name\' name="first_name" data-fv-field="first_name" value="{{ firstname }}" {{#field_lock_firstname}}disabled{{/field_lock_firstname}} >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} surname, theme_remui {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id=\'surname\' name="surname" data-fv-field="surname" value="{{ lastname }}" {{#field_lock_lastname}}disabled{{/field_lock_lastname}} >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} email, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" id=\'standard_email\' name="standard_email" data-fv-field="standard_email" value="{{ email }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} city, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id=\'city\' name="city" data-fv-field="city" value="{{#location}}{{city}} {{/location}}" {{#field_lock_city}}disabled{{/field_lock_city}}>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="col-md-3 form-control-label">{{# str }} country, moodle {{/ str }}</label>
                                                <div class="col-md-9">
                                                <select class="form-control" id="country" name="country" required="" data-fv-field="country" {{#field_lock_country}}disabled{{/field_lock_country}}>
                                                    {{# countries }}
                                                    <option value="{{ keyName }}">{{ valName }}</option>
                                                    {{/ countries }}
                                                </select>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 form-control-label">{{# str }} description, moodle {{/ str }}</label>
                                            <div class="col-md-9">
                                            <textarea class="form-control" id=\'description\' name="description" rows="3" data-fv-field="description">{{{description}}}</textarea>
                                            </div>
                                        </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary" id="btn-save-changes">{{# str }} submit, moodle {{/ str }}</button>
                                    </div>
                                </form>
                                    <!--<div class="alert alert-danger" id="error-message" style="display:none;margin-top:10px"></div>-->
                                    <div class="summary-errors alert alert-success alert-dismissible" id="error-message" style="display:none;margin-top:10px">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        {{/ notcurrentuser }}
                        {{/haseditpermission}}
                    </div>
                </div>
            </div>
            <!-- End Panel -->
        </div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                                        </div>
';
                $buffer .= $indent . '                                    </div>
';
                $buffer .= $indent . '                                </li>
';
                $buffer .= $indent . '                            </ul>
';
                $buffer .= $indent . '                        </div>
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                        <!-- edit profile tab content -->
';
                // 'haseditpermission' section
                $value = $context->find('haseditpermission');
                $buffer .= $this->section41463bad4ba39b6216f3907fb4193103($context, $indent, $value);
                $buffer .= $indent . '                    </div>
';
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '            <!-- End Panel -->
';
                $buffer .= $indent . '        </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section380b49d1abc26a2e8663155d70fddce3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
require([\'jquery\', \'theme_remui/loader\', \'theme_remui/profile\'], function (jquery) {
    Breakpoints();
    var tabs = {
        "aboutme": {
            "left": [],
            "right": ["usercourses", "editprofile", "userprefs"],
        },
        "usercourses": {
            "left": ["aboutme"],
            "right": ["editprofile", "userprefs"],
        },
        "editprofile": {
            "left": ["aboutme", "usercourses"],
            "right": ["userprefs"],
        },
        "userprefs": {
            "left": ["aboutme", "usercourses", "editprofile"],
            "right": [],
        },
    };
    $(\'.profile-tabs [role="tablist"] [role="tab"]\').click(function() {
        var prev = $(\'.profile-tabs [role="tablist"] [role="tab"].active\').attr(\'aria-controls\');
        var current = $(this).attr(\'aria-controls\');
        var anim = \'animation-slide-left\';
        if (tabs[current].left.indexOf(prev) != -1) {
            anim = \'animation-slide-right\';
        }
        $(\'.profile-tabs .tab-content [role="tabpanel"].tab-pane\').removeClass(\'animation-slide-left animation-slide-right\').addClass(anim);
    });
});
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . 'require([\'jquery\', \'theme_remui/loader\', \'theme_remui/profile\'], function (jquery) {
';
                $buffer .= $indent . '    Breakpoints();
';
                $buffer .= $indent . '    var tabs = {
';
                $buffer .= $indent . '        "aboutme": {
';
                $buffer .= $indent . '            "left": [],
';
                $buffer .= $indent . '            "right": ["usercourses", "editprofile", "userprefs"],
';
                $buffer .= $indent . '        },
';
                $buffer .= $indent . '        "usercourses": {
';
                $buffer .= $indent . '            "left": ["aboutme"],
';
                $buffer .= $indent . '            "right": ["editprofile", "userprefs"],
';
                $buffer .= $indent . '        },
';
                $buffer .= $indent . '        "editprofile": {
';
                $buffer .= $indent . '            "left": ["aboutme", "usercourses"],
';
                $buffer .= $indent . '            "right": ["userprefs"],
';
                $buffer .= $indent . '        },
';
                $buffer .= $indent . '        "userprefs": {
';
                $buffer .= $indent . '            "left": ["aboutme", "usercourses", "editprofile"],
';
                $buffer .= $indent . '            "right": [],
';
                $buffer .= $indent . '        },
';
                $buffer .= $indent . '    };
';
                $buffer .= $indent . '    $(\'.profile-tabs [role="tablist"] [role="tab"]\').click(function() {
';
                $buffer .= $indent . '        var prev = $(\'.profile-tabs [role="tablist"] [role="tab"].active\').attr(\'aria-controls\');
';
                $buffer .= $indent . '        var current = $(this).attr(\'aria-controls\');
';
                $buffer .= $indent . '        var anim = \'animation-slide-left\';
';
                $buffer .= $indent . '        if (tabs[current].left.indexOf(prev) != -1) {
';
                $buffer .= $indent . '            anim = \'animation-slide-right\';
';
                $buffer .= $indent . '        }
';
                $buffer .= $indent . '        $(\'.profile-tabs .tab-content [role="tabpanel"].tab-pane\').removeClass(\'animation-slide-left animation-slide-right\').addClass(anim);
';
                $buffer .= $indent . '    });
';
                $buffer .= $indent . '});
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
