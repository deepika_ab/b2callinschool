<?php

class __Mustache_ef2691b636c214499f01a6691c3c49df extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'iscoursestatsshow' section
        $value = $context->find('iscoursestatsshow');
        $buffer .= $this->sectionF47ca9728ab0520f9b26778bd3fc50ff($context, $indent, $value);

        return $buffer;
    }

    private function sectionF86102458a22ce89e5b4aa3d598fc41d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'enrolledusers, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'enrolledusers, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section0187c9e92c28d008078f9cc2f15ab639(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'studentcompleted, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'studentcompleted, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6ae4cf7e315fd753511f815971fc2eb5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'inprogress, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'inprogress, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section723daa7f4854466ef49cc23ac0189c7e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'yettostart, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'yettostart, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section96e3a0f5f650fac5f804180e9f9a2a88(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
	<div class="col-6 col-md-6 col-lg-3">
		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
			<p class="course-stats-completed display-4 bold mr-1 mb-0 mb-md-1">0</p>
			<p class="stats-title mb-0 mb-md-1">{{#str}}studentcompleted, theme_remui{{/str}}</p>
		</div>
	</div>
	<div class="col-6 col-md-6 col-lg-3">
		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
			<p class="course-stats-inprogress display-4 bold mr-1 mb-0 mb-md-1">0</p>
			<p class="stats-title mb-0 mb-md-1">{{#str}}inprogress, theme_remui{{/str}}</p>
		</div>
	</div>
	<div class="col-6 col-md-6 col-lg-3">
		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
			<p class="course-stats-notstarted display-4 bold mr-1 mb-0 mb-md-1">0</p>
			<p class="stats-title mb-0 mb-md-1">{{#str}}yettostart, theme_remui{{/str}}</p>
		</div>
	</div>
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '	<div class="col-6 col-md-6 col-lg-3">
';
                $buffer .= $indent . '		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
';
                $buffer .= $indent . '			<p class="course-stats-completed display-4 bold mr-1 mb-0 mb-md-1">0</p>
';
                $buffer .= $indent . '			<p class="stats-title mb-0 mb-md-1">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section0187c9e92c28d008078f9cc2f15ab639($context, $indent, $value);
                $buffer .= '</p>
';
                $buffer .= $indent . '		</div>
';
                $buffer .= $indent . '	</div>
';
                $buffer .= $indent . '	<div class="col-6 col-md-6 col-lg-3">
';
                $buffer .= $indent . '		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
';
                $buffer .= $indent . '			<p class="course-stats-inprogress display-4 bold mr-1 mb-0 mb-md-1">0</p>
';
                $buffer .= $indent . '			<p class="stats-title mb-0 mb-md-1">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section6ae4cf7e315fd753511f815971fc2eb5($context, $indent, $value);
                $buffer .= '</p>
';
                $buffer .= $indent . '		</div>
';
                $buffer .= $indent . '	</div>
';
                $buffer .= $indent . '	<div class="col-6 col-md-6 col-lg-3">
';
                $buffer .= $indent . '		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
';
                $buffer .= $indent . '			<p class="course-stats-notstarted display-4 bold mr-1 mb-0 mb-md-1">0</p>
';
                $buffer .= $indent . '			<p class="stats-title mb-0 mb-md-1">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->section723daa7f4854466ef49cc23ac0189c7e($context, $indent, $value);
                $buffer .= '</p>
';
                $buffer .= $indent . '		</div>
';
                $buffer .= $indent . '	</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF47ca9728ab0520f9b26778bd3fc50ff(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
<div id="wdm_course-stats" class="course-stats row mx-0 p-0 my-2">
	<div class="col-6 col-md-6 col-lg-3">
		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
			<p class="course-stats-enrolledusers display-4 bold mr-1 mb-0 mb-md-1">0</p>
			<p class="stats-title mb-0 mb-md-1">{{#str}}enrolledusers, theme_remui{{/str}}</p>
		</div>
	</div>
	{{#completion}}
	<div class="col-6 col-md-6 col-lg-3">
		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
			<p class="course-stats-completed display-4 bold mr-1 mb-0 mb-md-1">0</p>
			<p class="stats-title mb-0 mb-md-1">{{#str}}studentcompleted, theme_remui{{/str}}</p>
		</div>
	</div>
	<div class="col-6 col-md-6 col-lg-3">
		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
			<p class="course-stats-inprogress display-4 bold mr-1 mb-0 mb-md-1">0</p>
			<p class="stats-title mb-0 mb-md-1">{{#str}}inprogress, theme_remui{{/str}}</p>
		</div>
	</div>
	<div class="col-6 col-md-6 col-lg-3">
		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
			<p class="course-stats-notstarted display-4 bold mr-1 mb-0 mb-md-1">0</p>
			<p class="stats-title mb-0 mb-md-1">{{#str}}yettostart, theme_remui{{/str}}</p>
		</div>
	</div>
	{{/completion}}
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '<div id="wdm_course-stats" class="course-stats row mx-0 p-0 my-2">
';
                $buffer .= $indent . '	<div class="col-6 col-md-6 col-lg-3">
';
                $buffer .= $indent . '		<div class="stats-card d-flex align-items-center justify-content-center px-2 flex-wrap">
';
                $buffer .= $indent . '			<p class="course-stats-enrolledusers display-4 bold mr-1 mb-0 mb-md-1">0</p>
';
                $buffer .= $indent . '			<p class="stats-title mb-0 mb-md-1">';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionF86102458a22ce89e5b4aa3d598fc41d($context, $indent, $value);
                $buffer .= '</p>
';
                $buffer .= $indent . '		</div>
';
                $buffer .= $indent . '	</div>
';
                // 'completion' section
                $value = $context->find('completion');
                $buffer .= $this->section96e3a0f5f650fac5f804180e9f9a2a88($context, $indent, $value);
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
