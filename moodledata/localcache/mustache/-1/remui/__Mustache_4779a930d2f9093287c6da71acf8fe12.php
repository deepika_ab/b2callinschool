<?php

class __Mustache_4779a930d2f9093287c6da71acf8fe12 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '
';
        // 'addsection' section
        $value = $context->find('addsection');
        $buffer .= $this->sectionF023638b31344c216ff590b67b545030($context, $indent, $value);
        $buffer .= $indent . '
';
        // 'addsection' inverted section
        $value = $context->find('addsection');
        if (empty($value)) {
            
            $buffer .= $indent . '<li id="section-';
            $value = $this->resolveValue($context->find('index'), $context);
            $buffer .= $value;
            $buffer .= '" class="card section main ';
            // 'collapsed' section
            $value = $context->find('collapsed');
            $buffer .= $this->section7df0e231697f3a9dfdd1138e1b79a2b0($context, $indent, $value);
            $buffer .= ' clearfix mb-4 m-b-2" role="region" aria-labelledby="section-';
            $value = $this->resolveValue($context->find('index'), $context);
            $buffer .= call_user_func($this->mustache->getEscape(), $value);
            $buffer .= ' .sectionname a:not(.quickeditlink)">
';
            $buffer .= $indent . '    <div class="content card-block px-15 px-3 py-10 py-2 remui-list-content-border" aria-labelledby="section-';
            $value = $this->resolveValue($context->find('index'), $context);
            $buffer .= call_user_func($this->mustache->getEscape(), $value);
            $buffer .= ' .sectionname a:not(.quickeditlink)">
';
            $buffer .= $indent . '        ';
            // 'highlighted' section
            $value = $context->find('highlighted');
            $buffer .= $this->sectionF2366841dafb64da058bf610489c4292($context, $indent, $value);
            $buffer .= '
';
            $buffer .= $indent . '        <div class="d-block">
';
            $buffer .= $indent . '            <div class="left side">
';
            $buffer .= $indent . '                ';
            $value = $this->resolveValue($context->find('leftside'), $context);
            $buffer .= $value;
            $buffer .= '
';
            $buffer .= $indent . '            </div>
';
            $buffer .= $indent . '            <div class="right side d-flex">
';
            $buffer .= $indent . '                <button class="toggle-icon fa fa-angle-right"></button>
';
            $buffer .= $indent . '                ';
            $value = $this->resolveValue($context->find('addnewsection'), $context);
            $buffer .= $value;
            $buffer .= '
';
            $buffer .= $indent . '                ';
            $value = $this->resolveValue($context->find('optionmenu'), $context);
            $buffer .= $value;
            $buffer .= '
';
            $buffer .= $indent . '            </div>
';
            $buffer .= $indent . '            ';
            $value = $this->resolveValue($context->find('leftside'), $context);
            $buffer .= $value;
            $buffer .= '
';
            $buffer .= $indent . '        </div>
';
            $buffer .= $indent . '        <h4 class="sectionname card-title">
';
            // 'editing' section
            $value = $context->find('editing');
            $buffer .= $this->section1eef62257cb2082284fe64ff8994c5ff($context, $indent, $value);
            // 'editing' inverted section
            $value = $context->find('editing');
            if (empty($value)) {
                
                $buffer .= $indent . '                <a class="panel-title p-0" href="#sectionwrapper-';
                $value = $this->resolveValue($context->find('index'), $context);
                $buffer .= $value;
                $buffer .= '" aria-controls="sectionwrapper-';
                $value = $this->resolveValue($context->find('index'), $context);
                $buffer .= $value;
                $buffer .= '">';
                $value = $this->resolveValue($context->find('title'), $context);
                $buffer .= $value;
                $buffer .= '
';
            }
            // 'editing' inverted section
            $value = $context->find('editing');
            if (empty($value)) {
                
                // 'progressinfo' section
                $value = $context->find('progressinfo');
                $buffer .= $this->section36921aa725722d3a053ca37ba638a94b($context, $indent, $value);
                $buffer .= $indent . '                </a>
';
            }
            $buffer .= $indent . '        </h4>
';
            $buffer .= $indent . '        ';
            $value = $this->resolveValue($context->find('hiddenmessage'), $context);
            $buffer .= $value;
            $buffer .= '
';
            $buffer .= $indent . '        <div class="summary card-text">';
            $value = $this->resolveValue($context->find('summary'), $context);
            $buffer .= $value;
            $buffer .= '</div>
';
            $buffer .= $indent . '        <div class="section_availability badge badge-pill badge-info mb-10">';
            $value = $this->resolveValue($context->find('availability'), $context);
            $buffer .= $value;
            $buffer .= '</div>
';
            $buffer .= $indent . '        <div class="section-summary-activities mdl-right mb-10">';
            $value = $this->resolveValue($context->find('activityinfostring'), $context);
            $buffer .= $value;
            $buffer .= '</div>
';
            $buffer .= $indent . '        <div class="px-15 px-3 py-10 py-2 card-footer text-muted border-top" aria-labelledby="section-';
            $value = $this->resolveValue($context->find('index'), $context);
            $buffer .= call_user_func($this->mustache->getEscape(), $value);
            $buffer .= ' .sectionname a:not(.quickeditlink)" ';
            // 'collapsed' section
            $value = $context->find('collapsed');
            $buffer .= $this->sectionBb768792ca996fc7ab7cf64d2cb117e0($context, $indent, $value);
            $buffer .= '>
';
            $buffer .= $indent . '            ';
            $value = $this->resolveValue($context->find('sectionactivities'), $context);
            $buffer .= $value;
            $buffer .= '
';
            $buffer .= $indent . '        </div>
';
            $buffer .= $indent . '    </div>
';
            $buffer .= $indent . '</li>
';
        }

        return $buffer;
    }

    private function section57742138bdef30be2374d7f833a76e1e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <a href="{{{removeurl}}}" class="{{removeurlclass}}">{{{removeicon}}} {{{strremovesection}}}</a>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <a href="';
                $value = $this->resolveValue($context->find('removeurl'), $context);
                $buffer .= $value;
                $buffer .= '" class="';
                $value = $this->resolveValue($context->find('removeurlclass'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->find('removeicon'), $context);
                $buffer .= $value;
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('strremovesection'), $context);
                $buffer .= $value;
                $buffer .= '</a>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE8821f813b283b300fd52213603d2a29(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <a href="{{{addurl}}}" class="{{addurlclass}}">{{{addicon}}} {{{straddsection}}}</a>

            {{#strremovesection}}
            <a href="{{{removeurl}}}" class="{{removeurlclass}}">{{{removeicon}}} {{{strremovesection}}}</a>
            {{/strremovesection}}
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <a href="';
                $value = $this->resolveValue($context->find('addurl'), $context);
                $buffer .= $value;
                $buffer .= '" class="';
                $value = $this->resolveValue($context->find('addurlclass'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->find('addicon'), $context);
                $buffer .= $value;
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('straddsection'), $context);
                $buffer .= $value;
                $buffer .= '</a>
';
                $buffer .= $indent . '
';
                // 'strremovesection' section
                $value = $context->find('strremovesection');
                $buffer .= $this->section57742138bdef30be2374d7f833a76e1e($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF023638b31344c216ff590b67b545030(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
<li id="{{divid}}" class="card clearfix mb-4 m-b-2" role="region" style="">
    <div class="content card-block px-15 px-3 py-10 py-2 remui-list-content-border d-flex align-items-center">
        {{#numsections}}
            <a href="{{{addurl}}}" class="{{addurlclass}}">{{{addicon}}} {{{straddsection}}}</a>

            {{#strremovesection}}
            <a href="{{{removeurl}}}" class="{{removeurlclass}}">{{{removeicon}}} {{{strremovesection}}}</a>
            {{/strremovesection}}
        {{/numsections}}

        {{^numsections}}
            <a href="{{{url}}}" data-add-sections="{{{straddsections}}}">{{{icon}}} {{{straddsections}}}</a>
        {{/numsections}}
    </div>
</li>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '<li id="';
                $value = $this->resolveValue($context->find('divid'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" class="card clearfix mb-4 m-b-2" role="region" style="">
';
                $buffer .= $indent . '    <div class="content card-block px-15 px-3 py-10 py-2 remui-list-content-border d-flex align-items-center">
';
                // 'numsections' section
                $value = $context->find('numsections');
                $buffer .= $this->sectionE8821f813b283b300fd52213603d2a29($context, $indent, $value);
                $buffer .= $indent . '
';
                // 'numsections' inverted section
                $value = $context->find('numsections');
                if (empty($value)) {
                    
                    $buffer .= $indent . '            <a href="';
                    $value = $this->resolveValue($context->find('url'), $context);
                    $buffer .= $value;
                    $buffer .= '" data-add-sections="';
                    $value = $this->resolveValue($context->find('straddsections'), $context);
                    $buffer .= $value;
                    $buffer .= '">';
                    $value = $this->resolveValue($context->find('icon'), $context);
                    $buffer .= $value;
                    $buffer .= ' ';
                    $value = $this->resolveValue($context->find('straddsections'), $context);
                    $buffer .= $value;
                    $buffer .= '</a>
';
                }
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '</li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7df0e231697f3a9dfdd1138e1b79a2b0(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'collapsed';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'collapsed';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF2366841dafb64da058bf610489c4292(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '<div class="highlight bg-primary"></div>';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '<div class="highlight bg-primary"></div>';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1eef62257cb2082284fe64ff8994c5ff(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                {{{ title }}}
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                ';
                $value = $this->resolveValue($context->find('title'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE39b89b612ccbcb047d54841de3701f9(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <!-- Default checked -->
                    <div class="float-right">
                        <i class="fa fas fa-check-square text-success"></i>
                        <!-- {{{ progress }}} -->
                    </div>
                    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <!-- Default checked -->
';
                $buffer .= $indent . '                    <div class="float-right">
';
                $buffer .= $indent . '                        <i class="fa fas fa-check-square text-success"></i>
';
                $buffer .= $indent . '                        <!-- ';
                $value = $this->resolveValue($context->find('progress'), $context);
                $buffer .= $value;
                $buffer .= ' -->
';
                $buffer .= $indent . '                    </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section36921aa725722d3a053ca37ba638a94b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    {{#completed}}
                <!-- Default checked -->
                    <div class="float-right">
                        <i class="fa fas fa-check-square text-success"></i>
                        <!-- {{{ progress }}} -->
                    </div>
                    {{/completed}}
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'completed' section
                $value = $context->find('completed');
                $buffer .= $this->sectionE39b89b612ccbcb047d54841de3701f9($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionBb768792ca996fc7ab7cf64d2cb117e0(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'style="display: none;"';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'style="display: none;"';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
