<?php

class __Mustache_f0a1c9cd8fbdd25520ba2e757b6900ee extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<form action="';
        $value = $this->resolveValue($context->find('searchurl'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" id="';
        $value = $this->resolveValue($context->find('id'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" method="get" class="form-inline">
';
        $buffer .= $indent . '    <div class="input-group mb-3 coursesearchbox ">
';
        $buffer .= $indent . '        <input id="';
        $value = $this->resolveValue($context->find('inputid'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" type="text" name="q" size="';
        $value = $this->resolveValue($context->find('inputsize'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" value="';
        $value = $this->resolveValue($context->find('value'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" class="form-control" aria-labelledby="searchbtn" placeholder="';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionC348846e40dcc232dbc6c808ba6a6256($context, $indent, $value);
        $buffer .= '" >
';
        $buffer .= $indent . '        <input name="areaids" type="hidden" value="';
        $value = $this->resolveValue($context->find('areaids'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '">
';
        $buffer .= $indent . '        <div class="input-group-append">
';
        $buffer .= $indent . '            <button class="input-group-text bg-primary text-white rounded" aria-label="';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionC348846e40dcc232dbc6c808ba6a6256($context, $indent, $value);
        $buffer .= '"><i class="fa fa-search"></i></button>
';
        // 'helpicon' section
        $value = $context->find('helpicon');
        $buffer .= $this->sectionBd69c8ead33341c610cd0a90f1b26f0f($context, $indent, $value);
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</form>';

        return $buffer;
    }

    private function sectionC348846e40dcc232dbc6c808ba6a6256(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' searchcourses, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' searchcourses, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionBd69c8ead33341c610cd0a90f1b26f0f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            {{>core/help_icon}}
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                if ($partial = $this->mustache->loadPartial('core/help_icon')) {
                    $buffer .= $partial->renderInternal($context, $indent . '            ');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
