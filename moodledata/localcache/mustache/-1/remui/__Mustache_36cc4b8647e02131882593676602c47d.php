<?php

class __Mustache_36cc4b8647e02131882593676602c47d extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '<nav class="list-group" id="activity-nav">
';
        $buffer .= $indent . '    <a href="';
        $value = $this->resolveValue($context->find('courseurl'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '" class="list-group-item text-decoration-none">
';
        $buffer .= $indent . '        <div class="ml-0">
';
        $buffer .= $indent . '            <div class="media">
';
        $buffer .= $indent . '                <span class="media-left backtocourse">
';
        $buffer .= $indent . '                    <i class="icon fa fa-chevron-left" aria-hidden="true"></i>
';
        $buffer .= $indent . '                </span>
';
        $buffer .= $indent . '                <span class="media-body">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->sectionF9248b228f32b3dad9570fa771c47e16($context, $indent, $value);
        $buffer .= '</span>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </a>
';
        $buffer .= $indent . '    <p class="text-uppercase text-center font-size-10 justify-content-center align-items-center section-heading m-0">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section7b1ee928c0438ec92c011b7cb8aee4cb($context, $indent, $value);
        $buffer .= '</p>
';
        // 'activitysections' section
        $value = $context->find('activitysections');
        $buffer .= $this->section27ac0617a6a7f27ead3b174ceb55bb9f($context, $indent, $value);
        $buffer .= $indent . '
';
        // 'addblock' section
        $value = $context->find('addblock');
        $buffer .= $this->sectionBcdb18d4e0e17a2ef82f2601930b18fb($context, $indent, $value);
        $buffer .= $indent . '</nav>
';

        return $buffer;
    }

    private function sectionF9248b228f32b3dad9570fa771c47e16(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' backtocourse, theme_remui ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' backtocourse, theme_remui ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7b1ee928c0438ec92c011b7cb8aee4cb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'sections, theme_remui';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'sections, theme_remui';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section5749c750acb0d7477dd5257d00cc6d53(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'active';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'active';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section76a668ffe73e668faabc0958de4af85c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                <li class="list-group-item activity {{active}} {{ classes }} p-0" title="{{{name}}}">
                  {{{ title }}}
                </li>
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                <li class="list-group-item activity ';
                $value = $this->resolveValue($context->find('active'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' ';
                $value = $this->resolveValue($context->find('classes'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' p-0" title="';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= $value;
                $buffer .= '">
';
                $buffer .= $indent . '                  ';
                $value = $this->resolveValue($context->find('title'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '                </li>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section27ac0617a6a7f27ead3b174ceb55bb9f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
        <a href="javascript:void(0)" class="list-group-item activity-sections text-decoration-none {{#open}}active{{/open}}" data-toggle="collapse" data-target="#activity-submenu-{{count}}">
            <div class="ml-0">
                <div class="media">
                    <span class="media-body">{{{ name }}}<i class="icon fa fa-angle-right mr-0 ml-2" aria-hidden="true"></i></span>
                    <span class="media-left">
                        {{ count }}
                    </span>
                </div>
            </div>
        </a>
        <ul class="collapse p-0 {{open}} sub-menu m-0" id="activity-submenu-{{count}}" data-parent="#activiy-nav">
            {{# activity_list }}
                <li class="list-group-item activity {{active}} {{ classes }} p-0" title="{{{name}}}">
                  {{{ title }}}
                </li>
            {{/ activity_list }}
        </ul>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '        <a href="javascript:void(0)" class="list-group-item activity-sections text-decoration-none ';
                // 'open' section
                $value = $context->find('open');
                $buffer .= $this->section5749c750acb0d7477dd5257d00cc6d53($context, $indent, $value);
                $buffer .= '" data-toggle="collapse" data-target="#activity-submenu-';
                $value = $this->resolveValue($context->find('count'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">
';
                $buffer .= $indent . '            <div class="ml-0">
';
                $buffer .= $indent . '                <div class="media">
';
                $buffer .= $indent . '                    <span class="media-body">';
                $value = $this->resolveValue($context->find('name'), $context);
                $buffer .= $value;
                $buffer .= '<i class="icon fa fa-angle-right mr-0 ml-2" aria-hidden="true"></i></span>
';
                $buffer .= $indent . '                    <span class="media-left">
';
                $buffer .= $indent . '                        ';
                $value = $this->resolveValue($context->find('count'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '
';
                $buffer .= $indent . '                    </span>
';
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '        </a>
';
                $buffer .= $indent . '        <ul class="collapse p-0 ';
                $value = $this->resolveValue($context->find('open'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= ' sub-menu m-0" id="activity-submenu-';
                $value = $this->resolveValue($context->find('count'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" data-parent="#activiy-nav">
';
                // 'activity_list' section
                $value = $context->find('activity_list');
                $buffer .= $this->section76a668ffe73e668faabc0958de4af85c($context, $indent, $value);
                $buffer .= $indent . '        </ul>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionBcdb18d4e0e17a2ef82f2601930b18fb(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
        <a href="{{ action }}" class="list-group-item text-decoration-none" data-key="addblock">
            <div class="ml-0">
                <div class="media">
                    <span class="media-left">
                        <i class="icon fa fa-plus-circle" aria-hidden="true"></i>
                    </span>
                    <span class="media-body">{{{ text }}}</span>
                </div>
            </div>
        </a>
    ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '        <a href="';
                $value = $this->resolveValue($context->find('action'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '" class="list-group-item text-decoration-none" data-key="addblock">
';
                $buffer .= $indent . '            <div class="ml-0">
';
                $buffer .= $indent . '                <div class="media">
';
                $buffer .= $indent . '                    <span class="media-left">
';
                $buffer .= $indent . '                        <i class="icon fa fa-plus-circle" aria-hidden="true"></i>
';
                $buffer .= $indent . '                    </span>
';
                $buffer .= $indent . '                    <span class="media-body">';
                $value = $this->resolveValue($context->find('text'), $context);
                $buffer .= $value;
                $buffer .= '</span>
';
                $buffer .= $indent . '                </div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '        </a>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
