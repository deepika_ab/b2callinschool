<?php

class __Mustache_8598f3081a578dfc2009112a94d1e168 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        if ($partial = $this->mustache->loadPartial('theme_remui/common_start')) {
            $buffer .= $partial->renderInternal($context);
        }
        $buffer .= $indent . '
';
        $buffer .= $indent . '<section id="region-main" ';
        // 'hasblocks' section
        $value = $context->find('hasblocks');
        $buffer .= $this->sectionA8b30bc710534302fe5cc09141cbd5c7($context, $indent, $value);
        $buffer .= ' aria-label="';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section6b403a6a78537640b9e04a931aeb6463($context, $indent, $value);
        $buffer .= '">
';
        $buffer .= $indent . '    <div class="card card-body">
';
        // 'hasregionmainsettingsmenu' section
        $value = $context->find('hasregionmainsettingsmenu');
        $buffer .= $this->sectionA50e155453f858c6079de05dce02b047($context, $indent, $value);
        // 'notstudent' section
        $value = $context->find('notstudent');
        $buffer .= $this->section54936d8b2201878002c2fc5ad03e70ca($context, $indent, $value);
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.course_content_header'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.main_content'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.activity_navigation'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.course_content_footer'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '    </div> 
';
        $buffer .= $indent . '</section>
';
        $buffer .= $indent . '
';
        if ($partial = $this->mustache->loadPartial('theme_remui/common_end')) {
            $buffer .= $partial->renderInternal($context);
        }
        // 'js' section
        $value = $context->find('js');
        $buffer .= $this->section0e83ab11ae5e92e21bf2be24749bf97e($context, $indent, $value);

        return $buffer;
    }

    private function sectionA8b30bc710534302fe5cc09141cbd5c7(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'class="has-blocks mb-3"';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'class="has-blocks mb-3"';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6b403a6a78537640b9e04a931aeb6463(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'content';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'content';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA50e155453f858c6079de05dce02b047(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="region_main_settings_menu_proxy"></div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="region_main_settings_menu_proxy"></div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionCd04a7c04231a64ca4aee96868786a5f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                {{> theme_remui/course_stats }}
            ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                if ($partial = $this->mustache->loadPartial('theme_remui/course_stats')) {
                    $buffer .= $partial->renderInternal($context, $indent . '                ');
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section54936d8b2201878002c2fc5ad03e70ca(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            {{#iscoursestatsshow}}
                {{> theme_remui/course_stats }}
            {{/iscoursestatsshow}}
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'iscoursestatsshow' section
                $value = $context->find('iscoursestatsshow');
                $buffer .= $this->sectionCd04a7c04231a64ca4aee96868786a5f($context, $indent, $value);
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC5a26e57bba0e14c6770f8960ef627df(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            let courseid = getUrlParameter(\'id\');
            let service_name = \'theme_remui_get_course_stats\';
            let cstats = Ajax.call([
                {
                    methodname: service_name,
                    args: { courseid : courseid }
                }
            ]);
            cstats[0].done(function(response) {
                // When course stats block is ready then render the respose value
                $(\'.course-stats\').ready(function() {
                    $.each(response, function(index, value) {
                        var courseStatsDiv = \'#wdm_course-stats .stats-card .course-stats-\' + index;
                        $(courseStatsDiv).text(value);
                    })
                });
            });
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            let courseid = getUrlParameter(\'id\');
';
                $buffer .= $indent . '            let service_name = \'theme_remui_get_course_stats\';
';
                $buffer .= $indent . '            let cstats = Ajax.call([
';
                $buffer .= $indent . '                {
';
                $buffer .= $indent . '                    methodname: service_name,
';
                $buffer .= $indent . '                    args: { courseid : courseid }
';
                $buffer .= $indent . '                }
';
                $buffer .= $indent . '            ]);
';
                $buffer .= $indent . '            cstats[0].done(function(response) {
';
                $buffer .= $indent . '                // When course stats block is ready then render the respose value
';
                $buffer .= $indent . '                $(\'.course-stats\').ready(function() {
';
                $buffer .= $indent . '                    $.each(response, function(index, value) {
';
                $buffer .= $indent . '                        var courseStatsDiv = \'#wdm_course-stats .stats-card .course-stats-\' + index;
';
                $buffer .= $indent . '                        $(courseStatsDiv).text(value);
';
                $buffer .= $indent . '                    })
';
                $buffer .= $indent . '                });
';
                $buffer .= $indent . '            });
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section0e83ab11ae5e92e21bf2be24749bf97e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    require([\'jquery\', \'core/ajax\', \'theme_remui/loader\'], function ($, Ajax) {
        Breakpoints();
        // Topics click in sidebar 
        window.addEventListener("hashchange", function () {
            window.scrollTo(window.scrollX, window.scrollY - 80);
        });
        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split(\'&\'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split(\'=\');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }
        {{#iscoursestatsshow}}
            let courseid = getUrlParameter(\'id\');
            let service_name = \'theme_remui_get_course_stats\';
            let cstats = Ajax.call([
                {
                    methodname: service_name,
                    args: { courseid : courseid }
                }
            ]);
            cstats[0].done(function(response) {
                // When course stats block is ready then render the respose value
                $(\'.course-stats\').ready(function() {
                    $.each(response, function(index, value) {
                        var courseStatsDiv = \'#wdm_course-stats .stats-card .course-stats-\' + index;
                        $(courseStatsDiv).text(value);
                    })
                });
            });
        {{/iscoursestatsshow}}
    });
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    require([\'jquery\', \'core/ajax\', \'theme_remui/loader\'], function ($, Ajax) {
';
                $buffer .= $indent . '        Breakpoints();
';
                $buffer .= $indent . '        // Topics click in sidebar 
';
                $buffer .= $indent . '        window.addEventListener("hashchange", function () {
';
                $buffer .= $indent . '            window.scrollTo(window.scrollX, window.scrollY - 80);
';
                $buffer .= $indent . '        });
';
                $buffer .= $indent . '        function getUrlParameter(sParam) {
';
                $buffer .= $indent . '            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
';
                $buffer .= $indent . '            sURLVariables = sPageURL.split(\'&\'),
';
                $buffer .= $indent . '            sParameterName,
';
                $buffer .= $indent . '            i;
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '            for (i = 0; i < sURLVariables.length; i++) {
';
                $buffer .= $indent . '                sParameterName = sURLVariables[i].split(\'=\');
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '                if (sParameterName[0] === sParam) {
';
                $buffer .= $indent . '                    return sParameterName[1] === undefined ? true : sParameterName[1];
';
                $buffer .= $indent . '                }
';
                $buffer .= $indent . '            }
';
                $buffer .= $indent . '        }
';
                // 'iscoursestatsshow' section
                $value = $context->find('iscoursestatsshow');
                $buffer .= $this->sectionC5a26e57bba0e14c6770f8960ef627df($context, $indent, $value);
                $buffer .= $indent . '    });
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
