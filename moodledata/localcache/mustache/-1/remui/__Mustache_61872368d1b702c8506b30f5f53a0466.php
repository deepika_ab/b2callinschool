<?php

class __Mustache_61872368d1b702c8506b30f5f53a0466 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'card' section
        $value = $context->find('card');
        $buffer .= $this->section0f2a8355f3d1af763fc8d9d08177dfb0($context, $indent, $value);
        // 'list' section
        $value = $context->find('list');
        $buffer .= $this->section743caf4fe52f3390b00574754e5a4a95($context, $indent, $value);
        // 'summary' section
        $value = $context->find('summary');
        $buffer .= $this->sectionF07bb3a6977c30909422e616b5ca7ec1($context, $indent, $value);

        return $buffer;
    }

    private function section0f2a8355f3d1af763fc8d9d08177dfb0(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
<div data-region="loading-placeholder-content" aria-hidden="true">
    <div class="card-deck dashboard-card-deck one-row" style="height: 13rem">
        <div class="card border-0">
            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
            </div>
            <div class="card-body course-info-container px-0">
                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
            </div>
        </div>
        <div class="card border-0">
            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
            </div>
            <div class="card-body course-info-container px-0">
                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
            </div>
        </div>
        <div class="card border-0">
            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
            </div>
            <div class="card-body course-info-container px-0">
                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
            </div>
        </div>
        <div class="card border-0">
            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
            </div>
            <div class="card-body course-info-container px-0">
                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
            </div>
        </div>
    </div>
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '<div data-region="loading-placeholder-content" aria-hidden="true">
';
                $buffer .= $indent . '    <div class="card-deck dashboard-card-deck one-row" style="height: 13rem">
';
                $buffer .= $indent . '        <div class="card border-0">
';
                $buffer .= $indent . '            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '            <div class="card-body course-info-container px-0">
';
                $buffer .= $indent . '                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '        <div class="card border-0">
';
                $buffer .= $indent . '            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '            <div class="card-body course-info-container px-0">
';
                $buffer .= $indent . '                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '        <div class="card border-0">
';
                $buffer .= $indent . '            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '            <div class="card-body course-info-container px-0">
';
                $buffer .= $indent . '                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '        <div class="card border-0">
';
                $buffer .= $indent . '            <div class="card-img-top bg-pulse-grey w-100" style="height: 7rem">
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '            <div class="card-body course-info-container px-0">
';
                $buffer .= $indent . '                <div class="bg-pulse-grey w-100" style="height: 1rem"></div>
';
                $buffer .= $indent . '            </div>
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section743caf4fe52f3390b00574754e5a4a95(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
<div class="w-p100" data-region="loading-placeholder-content" aria-hidden="true">
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
    </div>
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
    </div>
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
    </div>
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
    </div>
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '<div class="w-p100" data-region="loading-placeholder-content" aria-hidden="true">
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 4rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF07bb3a6977c30909422e616b5ca7ec1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
<div class="w-p100" data-region="loading-placeholder-content" aria-hidden="true">
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
    </div>
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
    </div>
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
    </div>
    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
    </div>
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '<div class="w-p100" data-region="loading-placeholder-content" aria-hidden="true">
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '    <div class="bg-pulse-grey w-p100 m-b-1" style="height: 10rem">
';
                $buffer .= $indent . '    </div>
';
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
