<?php

class __Mustache_8e6a2b0ea9c9ed4877274afa6ad42a80 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '<div class="efb-wrap-list efb-wrap-list-forms">
';
        $buffer .= $indent . '    <div class=\'efb-list-buttons text-right\'>
';
        // 'pageactions' section
        $value = $context->find('pageactions');
        $buffer .= $this->section2e2c82fca47960bef5edce4c138f088a($context, $indent, $value);
        $buffer .= '    </div>
';
        $buffer .= $indent . '    <hr>
';
        $buffer .= $indent . '    <h2>';
        $value = $this->resolveValue($context->find('heading'), $context);
        $buffer .= call_user_func($this->mustache->getEscape(), $value);
        $buffer .= '</h2>
';
        $buffer .= $indent . '    <table id="efb-forms" class="efb-table efb-forms-table">
';
        $buffer .= $indent . '        <thead>
';
        $buffer .= $indent . '            <tr>
';
        // 'headings' section
        $value = $context->find('headings');
        $buffer .= $this->section8431062f60fd7089d6740cd4d2a92faa($context, $indent, $value);
        $buffer .= $indent . '            </tr>
';
        $buffer .= $indent . '        </thead>
';
        $buffer .= $indent . '    </table>
';
        $buffer .= $indent . '    <div class="efb-modal" id="efb-modal" data-region="modal-container" aria-hidden="false" role="dialog">
';
        $buffer .= $indent . '        <div class="efb-modal-dialog " role="document" data-region="efb-modal" aria-labelledby="17-modal-title">
';
        $buffer .= $indent . '            <div class="efb-modal-content">
';
        $buffer .= $indent . '                <div class="efb-modal-header" data-region="header">
';
        $buffer .= $indent . '                    <h4 id="efb-modal-title" class="efb-modal-title text-white" data-region="title" tabindex="0"></h4>
';
        $buffer .= $indent . '                    <button type="button" class="close efb-modal-close text-white" data-action="hide" aria-label="Close">
';
        $buffer .= $indent . '                      <span aria-hidden="true">×</span>
';
        $buffer .= $indent . '                    </button>
';
        $buffer .= $indent . '                </div>
';
        $buffer .= $indent . '                <div class="efb-modal-body" data-region="body">
';
        $buffer .= $indent . '                </div>
';
        $buffer .= $indent . '                <div class="efb-modal-footer" data-region="footer">
';
        $buffer .= $indent . '                    <button class="btn btn-danger efb-modal-delete-form" type="button">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section1a3e2025f7032830b53ef2acfb57e44e($context, $indent, $value);
        $buffer .= '</button>
';
        $buffer .= $indent . '                    <button class="btn btn-success efb-modal-pro-activate" type="button">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section6bbcf7c9ef113ca89879567e0984f8e1($context, $indent, $value);
        $buffer .= '</button>
';
        $buffer .= $indent . '                    <button class="btn btn-success efb-modal-close efb-modal-delete-close" type="button">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section4ec3d5a82e15e483e5d4a89a0d9920a3($context, $indent, $value);
        $buffer .= '</button>
';
        $buffer .= $indent . '                    <button class="btn btn-warning efb-modal-close efb-modal-pro-activate-close" type="button">';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section4ec3d5a82e15e483e5d4a89a0d9920a3($context, $indent, $value);
        $buffer .= '</button>
';
        $buffer .= $indent . '                </div>
';
        $buffer .= $indent . '            </div>
';
        $buffer .= $indent . '        </div>
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }

    private function section2e2c82fca47960bef5edce4c138f088a(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            {{< local_edwiserform/header-action-buttons}}
                    {{$ button-class }}efb-list-button{{/ button-class }}
                {{/ local_edwiserform/header-action-buttons}}
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            ';
                if ($parent = $this->mustache->loadPartial('local_edwiserform/header-action-buttons')) {
                    $context->pushBlockContext(array(
                        'button-class' => array($this, 'blockF5064c023a00ed9574a5436253d42553'),
                    ));
                    $buffer .= $parent->renderInternal($context, $indent);
                    $context->popBlockContext();
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section8431062f60fd7089d6740cd4d2a92faa(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    <th>{{{.}}}</th>
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    <th>';
                $value = $this->resolveValue($context->last(), $context);
                $buffer .= $value;
                $buffer .= '</th>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section1a3e2025f7032830b53ef2acfb57e44e(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' proceed, local_edwiserform ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' proceed, local_edwiserform ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6bbcf7c9ef113ca89879567e0984f8e1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' activatelicense, local_edwiserform ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' activatelicense, local_edwiserform ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4ec3d5a82e15e483e5d4a89a0d9920a3(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' cancel, local_edwiserform ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' cancel, local_edwiserform ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    public function blockF5064c023a00ed9574a5436253d42553($context)
    {
        $indent = $buffer = '';
        $buffer .= 'efb-list-button';
    
        return $buffer;
    }
}
