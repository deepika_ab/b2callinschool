<?php

class __Mustache_912dacb223fd58042bec8711cf571214 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        if ($partial = $this->mustache->loadPartial('theme_remui/common_start')) {
            $buffer .= $partial->renderInternal($context);
        }
        $buffer .= $indent . '
';
        $buffer .= $indent . '<section id="region-main" ';
        // 'hasblocks' section
        $value = $context->find('hasblocks');
        $buffer .= $this->sectionA8b30bc710534302fe5cc09141cbd5c7($context, $indent, $value);
        $buffer .= ' aria-label="';
        // 'str' section
        $value = $context->find('str');
        $buffer .= $this->section6b403a6a78537640b9e04a931aeb6463($context, $indent, $value);
        $buffer .= '">
';
        $buffer .= $indent . '    <div class="card card-body">
';
        // 'hasregionmainsettingsmenu' section
        $value = $context->find('hasregionmainsettingsmenu');
        $buffer .= $this->sectionA50e155453f858c6079de05dce02b047($context, $indent, $value);
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.course_content_header'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.main_content'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.activity_navigation'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->findDot('output.course_content_footer'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</section>
';
        $buffer .= $indent . '
';
        if ($partial = $this->mustache->loadPartial('theme_remui/common_end')) {
            $buffer .= $partial->renderInternal($context);
        }
        // 'js' section
        $value = $context->find('js');
        $buffer .= $this->sectionE3c1219033f56b03691d0c711ba1204a($context, $indent, $value);

        return $buffer;
    }

    private function sectionA8b30bc710534302fe5cc09141cbd5c7(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'class="has-blocks mb-3"';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'class="has-blocks mb-3"';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6b403a6a78537640b9e04a931aeb6463(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'content';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'content';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA50e155453f858c6079de05dce02b047(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <div class="region_main_settings_menu_proxy"></div>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <div class="region_main_settings_menu_proxy"></div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE3c1219033f56b03691d0c711ba1204a(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
require([\'theme_remui/loader\', \'theme_remui/TimeCircles\'], function (loader, TimeCircles) {
    Breakpoints();

    // quiz time circles for timed quizzes
    jQuery("#quiztimer").TimeCircles({
        time: {
            Days: {
                show: false
            },
            Hours: {
                color: "#3c8dbc"
            },
            Minutes: {
                color: "#00a65a"
            },
            Seconds: {
                color: "#f56954"
            }
        },
        bg_width: 0.9,
        fg_width: 0.1,
        circle_bg_color: "#797D82",
        number_size: 0.24,
        text_size: 0.11,
        refresh_interval: 1,
        animation_interval: "ticks"
    }).addListener(quizTimeEllapsed);

    // listner for quiz timer
    function quizTimeEllapsed(unit, value, total) {
        if (total <= 0) {
            jQuery(this).fadeOut(\'medium\').replaceWith(\'<div style="text-align: center; background: rgba(0, 0, 0, 0.13); border-radius: 5px; height: 80px; line-height: 80px; font-size: 18px; color: red;">\' + M.util.get_string(\'timesup\', \'quiz\') + \'</div>\');
        }
    }
});
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . 'require([\'theme_remui/loader\', \'theme_remui/TimeCircles\'], function (loader, TimeCircles) {
';
                $buffer .= $indent . '    Breakpoints();
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '    // quiz time circles for timed quizzes
';
                $buffer .= $indent . '    jQuery("#quiztimer").TimeCircles({
';
                $buffer .= $indent . '        time: {
';
                $buffer .= $indent . '            Days: {
';
                $buffer .= $indent . '                show: false
';
                $buffer .= $indent . '            },
';
                $buffer .= $indent . '            Hours: {
';
                $buffer .= $indent . '                color: "#3c8dbc"
';
                $buffer .= $indent . '            },
';
                $buffer .= $indent . '            Minutes: {
';
                $buffer .= $indent . '                color: "#00a65a"
';
                $buffer .= $indent . '            },
';
                $buffer .= $indent . '            Seconds: {
';
                $buffer .= $indent . '                color: "#f56954"
';
                $buffer .= $indent . '            }
';
                $buffer .= $indent . '        },
';
                $buffer .= $indent . '        bg_width: 0.9,
';
                $buffer .= $indent . '        fg_width: 0.1,
';
                $buffer .= $indent . '        circle_bg_color: "#797D82",
';
                $buffer .= $indent . '        number_size: 0.24,
';
                $buffer .= $indent . '        text_size: 0.11,
';
                $buffer .= $indent . '        refresh_interval: 1,
';
                $buffer .= $indent . '        animation_interval: "ticks"
';
                $buffer .= $indent . '    }).addListener(quizTimeEllapsed);
';
                $buffer .= $indent . '
';
                $buffer .= $indent . '    // listner for quiz timer
';
                $buffer .= $indent . '    function quizTimeEllapsed(unit, value, total) {
';
                $buffer .= $indent . '        if (total <= 0) {
';
                $buffer .= $indent . '            jQuery(this).fadeOut(\'medium\').replaceWith(\'<div style="text-align: center; background: rgba(0, 0, 0, 0.13); border-radius: 5px; height: 80px; line-height: 80px; font-size: 18px; color: red;">\' + M.util.get_string(\'timesup\', \'quiz\') + \'</div>\');
';
                $buffer .= $indent . '        }
';
                $buffer .= $indent . '    }
';
                $buffer .= $indent . '});
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
